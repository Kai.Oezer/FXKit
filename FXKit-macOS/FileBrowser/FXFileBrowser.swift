//
//  FXFileBrowser.swift
//  FXKit
//
//  Created by Kai Özer on 7/21/14.
//  Copyright (c) 2014-2021 Kai Özer. All rights reserved.
//
import Cocoa

enum FXFileBrowserColumn : String
{
	case name = "name"
	case modified = "modified"
	case size = "size"

	var itemIdentifier : NSUserInterfaceItemIdentifier { NSUserInterfaceItemIdentifier(rawValue:self.rawValue) }
}

extension NSUserInterfaceItemIdentifier
{
	static let fileBrowserColumnName = FXFileBrowserColumn.name.itemIdentifier
	static let fileBrowserColumnSize = FXFileBrowserColumn.size.itemIdentifier
	static let fileBrowserColumnModified = FXFileBrowserColumn.modified.itemIdentifier
}


@objc public class FXFileBrowser : NSViewController,
  FXFileBrowserTableViewClient,
  NSTableViewDataSource,
  NSTableViewDelegate,
  NSTextFieldDelegate,
  FXFolderChangeObserver
{
  @IBOutlet weak var fileTable : FXFileBrowserTableView?
  private var _files = [FXFileBrowserFileData]()
  private var _fileEventStream : FSEventStreamRef?
  private var _handlingDeleteKey : Bool = false

  public init()
  {
    super.init(nibName:"FXFileBrowser", bundle:Bundle(for:Self.self))
  }

  public convenience required init?(coder:NSCoder)
  {
    self.init()
  }

  deinit
  {
    NotificationCenter.default.removeObserver(self)
  }

  // MARK: NSViewController overrides

  override public func awakeFromNib()
  {
    _initializeUI()
  }

  //MARK: Public

  @objc public var rootFolderLocation : URL? = nil
  {
    didSet {
      refresh()
      _setUpFolderMonitoring()
    }
  }

  public func refresh()
  {
    _buildFolderData()
    self.fileTable?.reloadData()
  }

  // @param fileNames the names of the items to be highlighted
  @objc public func highlight(_ fileNames:[String])
  {
    var highlightedItems = IndexSet()
    for (index, fileData) in _files.enumerated()
    {
      if fileNames.contains(fileData.fileName)
      {
        highlightedItems.insert(index)
      }
    }
    if !highlightedItems.isEmpty
    {
      self.fileTable?.selectRowIndexes(highlightedItems, byExtendingSelection:false)
    }
  }

  @objc func handleTableViewDeleteKey(notification:NSNotification)
  {
    let selectedRows = self.fileTable!.selectedRowIndexes
    if selectedRows.count > 0
    {
      _handlingDeleteKey = true
      _showAlert(message: FXLocString("FileBrowserAlert_DeleteSelected_Message"),
        acceptTitle:FXLocString("FileBrowserAlert_DeleteSelected_Accept"),
        abortTitle:FXLocString("FileBrowserAlert_Cancel"),
        sourceLocation:nil)
    }
  }

  //MARK: NSResponder overwrites

  override public func encodeRestorableState(with state:NSCoder)
  {
    super.encodeRestorableState(with: state)
    if let table = fileTable
    {
      table.encodeRestorableState(with: state)
      let encodeColumnWidth : (FXFileBrowserColumn)->() = { column in
				state.encode(Float((table.tableColumn(withIdentifier: column.itemIdentifier)!.width)), forKey:column.rawValue)
			}
			encodeColumnWidth(.name)
			encodeColumnWidth(.modified)
			encodeColumnWidth(.size)
    }
  }

  override public func restoreState(with state:NSCoder)
  {
    super.restoreState(with: state)
    if let table = fileTable
    {
      table.restoreState(with: state)
      let restoreColumnWidth : (FXFileBrowserColumn)->() = { column in
				if state.containsValue(forKey: column.rawValue)
				{
					table.tableColumn(withIdentifier: column.itemIdentifier)?.width = CGFloat(state.decodeFloat(forKey: column.rawValue))
				}
			}
			restoreColumnWidth(.name)
			restoreColumnWidth(.size)
			restoreColumnWidth(.modified)
    }
  }


  //MARK: FXFileBrowserTableViewClient

	func handleDroppedFiles(_ fileURLs:[URL]) -> Bool
  {
		guard let root = rootFolderLocation else { return false }
    var handled = false
    let fm = FileManager.default
    for fileURL in fileURLs
    {
      var isDir : ObjCBool = false
      if fm.fileExists(atPath: fileURL.path, isDirectory:&isDir) && !isDir.boolValue
      {
				let sourceFileName = fileURL.lastPathComponent
				let destinationPath = root.appendingPathComponent(sourceFileName).path
				if fm.fileExists(atPath: destinationPath)
				{
					_handlingDeleteKey = false
					_showAlert(message: String(format:FXLocString("FileBrowserAlert_OverwriteFile_Message"), sourceFileName),
						acceptTitle:FXLocString("FileBrowserAlert_OverwriteFile_Accept"),
						abortTitle:FXLocString("FileBrowserAlert_Cancel"),
						sourceLocation:fileURL)
				}
				else
				{
					_copyFile(from: fileURL)
				}
				handled = true
      }
    }
    refresh()
    return handled
  }


  //MARK: NSTableViewDataSource

  public func numberOfRowsInTableView(aTableView: NSTableView) -> Int
  {
    _files.count
  }

  public func tableView(_ aTableView: NSTableView, validateDrop info: NSDraggingInfo, proposedRow row: Int, proposedDropOperation operation: NSTableView.DropOperation) -> NSDragOperation
  {
    guard let draggedItems = info.draggingPasteboard.pasteboardItems, !draggedItems.isEmpty else { return [] }
    guard let source = info.draggingSource as? FXFileBrowserTableView, source === self.fileTable else { return [] }
		guard info.draggingSourceOperationMask.contains(.copy) else { return [] }
		return _validateDropped(items: draggedItems) ? .copy : []
  }

  public func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row rowIndex: Int) -> Any?
  {
		guard tableView === self.fileTable,
			(rowIndex >= 0) && (rowIndex < _files.count) else { return nil }

		let fileData = _files[rowIndex] as FXFileBrowserFileData
		guard let columnID = tableColumn?.identifier else { return nil }
		if columnID == .fileBrowserColumnName
		{
			return fileData.fileName
		}
		else if columnID == .fileBrowserColumnSize
		{
			return _fileSizeString(for: fileData.sizeInBytes)
		}
		else if columnID == .fileBrowserColumnModified
		{
			if let date = fileData.lastModified
			{
				return _dateString(for: date)
			}
		}
		return nil
  }

  public func tableView(_ tableView: NSTableView, pasteboardWriterForRow row: Int) -> NSPasteboardWriting?
  {
    return _files[row]
  }

  public func tableView(_ tableView: NSTableView, draggingSession session: NSDraggingSession, willBeginAt screenPoint: NSPoint, forRowIndexes rowIndexes: IndexSet)
  {
    session.enumerateDraggingItems(options: [], for:tableView, classes:[FXFileBrowserFileData.self, NSPasteboardItem.self], searchOptions:[:], using:{ draggingItem, index, stop in
      if let item = draggingItem.item as? FXFileBrowserFileData,
        let tableCellView = tableView.makeView(withIdentifier: .fileBrowserColumnName, owner:self) as? NSTableCellView
      {
        tableCellView.textField?.stringValue = item.fileName
        draggingItem.imageComponentsProvider = { return tableCellView.draggingImageComponents }
      }
      else
      {
        debugPrint("Something is fishy. The dragged item should be a FileBrowserFileData instance.")
        draggingItem.imageComponentsProvider = nil;
      }
    })
  }


  //MARK: NSTableViewDelegate

  public func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView?
  {
    guard let columnIdentifier = tableColumn?.identifier else { return nil }
    guard let cellView = tableView.makeView(withIdentifier: columnIdentifier, owner:self) as? NSTableCellView else { return nil }
    let fileData = _files[row]
    if columnIdentifier == .fileBrowserColumnName
    {
      cellView.textField?.stringValue = fileData.fileName
      cellView.textField?.isEditable = true
      cellView.textField?.delegate = self
    }
    else if columnIdentifier == .fileBrowserColumnSize
    {
      cellView.textField?.stringValue = _fileSizeString(for: fileData.sizeInBytes)
    }
    else if columnIdentifier == .fileBrowserColumnModified
    {
      if let lastMod = fileData.lastModified
      {
        cellView.textField?.stringValue = _dateString(for: lastMod)
      }
			else
      {
        cellView.textField?.stringValue = ""
      }
    }
    return cellView
  }

  //MARK: NSTextFieldDelegate (NSControlTextEditingDelegate)

  public func controlTextDidEndEditing(_ notification: Notification)
  {
    guard let editedTextField = notification.object as? NSTextField else { return }
    guard let rowIndex = self.fileTable?.row(for: editedTextField) else { return }
    guard rowIndex >= 0 else { return }
    let fileData = _files[rowIndex]
    let fileName = fileData.fileName
    guard fileName != editedTextField.stringValue else { return }
    guard let originalLocation = self.rootFolderLocation?.appendingPathComponent(fileName) else { return }
    guard let newLocation = self.rootFolderLocation?.appendingPathComponent(editedTextField.stringValue) else { return }
    do
    {
      try FileManager.default.moveItem(at: originalLocation, to: newLocation)
      refresh()
    }
    catch let err
    {
      self.view.window?.presentError(err)
    }
  }


  //MARK: FXFolderChangeObserver

  public func handleFolderContentsChange()
  {
    refresh()
  }


  //MARK: Private methods

  private func _showAlert(message : String, acceptTitle : String, abortTitle : String, sourceLocation : URL?)
  {
    let alert = NSAlert()
    alert.addButton(withTitle: acceptTitle) // first button
    alert.addButton(withTitle: abortTitle)
    alert.messageText = message
    alert.alertStyle = .warning
    alert.beginSheetModal(for: self.view.window!) { returnCode in
      alert.window.orderOut(self)
      guard returnCode == .alertFirstButtonReturn else { return }
			if self._handlingDeleteKey
			{
				self._deleteSelectedFiles()
			}
			else if let fileCopySource = sourceLocation
			{
				self._copyFile(from: fileCopySource)
			}
    }
  }

  private func _copyFile(from fileLocation:URL)
  {
		guard let destinationLocation = self.rootFolderLocation?.appendingPathComponent(fileLocation.lastPathComponent) else { return }
		let fm = FileManager.default
    do
    {
      if fm.fileExists(atPath: destinationLocation.path)
      {
        try fm.removeItem(at: destinationLocation)
      }
      try fm.copyItem(at: fileLocation, to: destinationLocation)
    }
    catch let err { self.view.window?.presentError(err) }
  }

  private func _initializeUI()
  {
    guard let table : FXFileBrowserTableView = self.fileTable else { return }
		((table.tableColumn(withIdentifier: .fileBrowserColumnName)?.headerCell)! as NSCell).stringValue = NSLocalizedString("FileBrowserColumnTitle_Name", tableName:"FXKit", comment:"")
		((table.tableColumn(withIdentifier: .fileBrowserColumnModified)?.headerCell)! as NSCell).stringValue = NSLocalizedString("FileBrowserColumnTitle_LastModified", tableName:"FXKit", comment:"")
		((table.tableColumn(withIdentifier: .fileBrowserColumnSize)?.headerCell)! as NSCell).stringValue = NSLocalizedString("FileBrowserColumnTitle_Size", tableName:"FXKit", comment:"")
		table.columnAutoresizingStyle = .noColumnAutoresizing
		table.delegate = self
		table.dataSource = self
		table.client = self
		NotificationCenter.default.addObserver(self, selector: #selector(handleTableViewDeleteKey), name:Notification.Name(rawValue: FXTableView.DeleteKeyNotification), object:table)
  }

  private func _buildFolderData()
  {
    _files.removeAll()
		guard let root = self.rootFolderLocation else { return }
    let fm = FileManager.default
		guard let fileURLs = try? fm.contentsOfDirectory(at: root, includingPropertiesForKeys:nil, options:FileManager.DirectoryEnumerationOptions(rawValue: 0)) else { return }
		for fileURL in fileURLs
		{
			if fileURL.isFileURL && !fileURL.lastPathComponent.hasPrefix(".") // i.e., if it is not a hidden file
			{
				let filePath = fileURL.path
				if let fileAttributes = try? fm.attributesOfItem(atPath:filePath),
					let fileSize = (fileAttributes[.size] as? NSNumber)?.uint64Value,
					let modificationDate = fileAttributes[.modificationDate] as? Date
					{
						let fileData = FXFileBrowserFileData(fileLocation:fileURL, modificationDate:modificationDate, fileSize:UInt(fileSize))
						_files.append(fileData)
					}
			}
		}
		_files.sort(by: { $0 < $1 })  // mFiles.sort { (first:FileBrowserFileData, second:FileBrowserFileData) -> Bool in return first < second }
  }

  private func _fileSizeString(for fileSize : UInt) -> String
  {
    let KiloByte = UInt(1024)
    let MegaByte = KiloByte * KiloByte
    if fileSize < KiloByte
    {
      return "\(fileSize)"
    }
    if fileSize < MegaByte
    {
      let kiloBytes = fileSize / KiloByte
      return "\(kiloBytes) KB"
    }
    let megaBytes = fileSize / MegaByte
    return "\(megaBytes) MB"
  }

  private func _dateString(for date: Date) -> String
  {
    let dateFormatter = DateFormatter()
    dateFormatter.dateStyle = .medium
    dateFormatter.timeStyle = .short
    dateFormatter.locale = Locale.current
    return dateFormatter.string(from:date)
  }

  private func _setUpFolderMonitoring()
  {
    if let root = rootFolderLocation, let eventStream = _fileEventStream
    {
      FXSystemUtils.setUpFolderMonitoringForLocation(root, eventStream:eventStream, observer:self)
    }
  }

  private func _deleteSelectedFiles()
  {
    guard let root = self.rootFolderLocation else { return }
    guard let selectedRows = self.fileTable?.selectedRowIndexes else { return }
		let fm = FileManager.default
    for rowIndex in selectedRows.reversed()
    {
			let fileName = _files[rowIndex].fileName
			do { try fm.removeItem(at: root.appendingPathComponent(fileName)) }
			catch {}
		}
    refresh()
  }

  private func _validateDropped(items:[NSPasteboardItem]) -> Bool
  {
    var hasValidItem : Bool = false
		var isDir : ObjCBool = false
		let fm = FileManager.default
    for item in items
    {
      if let availableType = item.availableType(from: [.fileURL]),
				let fileURLString = item.string(forType: availableType),
				let fileURL = NSURL(string:fileURLString),
				let filePath = fileURL.path,
        fm.fileExists(atPath: filePath, isDirectory:&isDir) && !isDir.boolValue
				{
					hasValidItem = true
					break;
				}
    }
    return hasValidItem;
  }

}
