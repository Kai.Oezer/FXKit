//
//  FileBrowserTableView.swift
//  FXKit
//
//  Created by Kai Özer on 7/21/14.
//  Copyright (c) 2014-2021 Kai Özer. All rights reserved.
//
import Cocoa

protocol FXFileBrowserTableViewClient
{
  func handleDroppedFiles(_ files : [URL]) -> Bool
}

@objc class FXFileBrowserTableView : FXTableView
{
  var client : FXFileBrowserTableViewClient? = nil

  override func awakeFromNib()
  {
    registerForDraggedTypes([.fileURL])
    setDraggingSourceOperationMask(.copy, forLocal:false)
  }

  //MARK: NSDraggingDestination

  override func prepareForDragOperation(_ info : NSDraggingInfo) -> Bool
  {
    guard let droppedItems = info.draggingPasteboard.pasteboardItems else { return false }
    for item in droppedItems
    {
      if item.types.contains(where:{ $0 == .fileURL })
      {
				return true
      }
    }
    return false
  }

  override func performDragOperation(_ info : NSDraggingInfo) -> Bool
  {
    guard let droppedItems = info.draggingPasteboard.pasteboardItems else { return false }
    var fileURLs = [URL]()
    for item in droppedItems
    {
      if item.types.contains(where: {$0 == .fileURL})
      {
        if let URLString = item.string(forType: .fileURL),
          let fileLocation = URL(string:URLString)
        {
          fileURLs.append(fileLocation)
        }
      }
    }
    if let client = self.client, !fileURLs.isEmpty
		{
			return client.handleDroppedFiles(fileURLs)
		}
    return false
  }

}
