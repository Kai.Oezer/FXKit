//
//  FXFileBrowserFileData.swift
//  FXKit
//
//  Created by Kai Özer on 7/21/14.
//  Copyright (c) 2014-2021 Kai Özer. All rights reserved.
//
import Cocoa

class FXFileBrowserFileData : NSObject, NSSecureCoding, NSCopying, NSPasteboardWriting, NSPasteboardReading
{
	private(set) var fileName : String
	private(set) var lastModified : Date?
	private(set) var sizeInBytes : UInt = 0
	private(set) var location : URL

	init(fileLocation : URL, modificationDate : Date?, fileSize : UInt)
	{
		self.location = fileLocation
		self.fileName = fileLocation.lastPathComponent
		self.lastModified = modificationDate
		self.sizeInBytes = fileSize
	}

	func compare(_ other : FXFileBrowserFileData) -> ComparisonResult
	{
		self.fileName.localizedCaseInsensitiveCompare(other.fileName as String)
	}

	override var description : String
	{
		String(describing:self.location.path)
	}

	//MARK: NSPasteboardWriting

	func writableTypes(for pasteboard: NSPasteboard) -> [NSPasteboard.PasteboardType]
	{
		(self.location as NSURL).writableTypes(for: pasteboard)
	}

	func writingOptions(forType type: NSPasteboard.PasteboardType, pasteboard: NSPasteboard) -> NSPasteboard.WritingOptions
	{
		(self.location as NSURL).writingOptions(forType: type, pasteboard:pasteboard)
	}

	func pasteboardPropertyList(forType type: NSPasteboard.PasteboardType) -> Any?
	{
		(self.location as NSURL).pasteboardPropertyList(forType:type)
	}

  //MARK: NSPasteboardReading

  static func readableTypes(for pasteboard: NSPasteboard) -> [NSPasteboard.PasteboardType]
  {
    return [.fileURL, .URL]
  }

  static func readingOptions(forType type: NSPasteboard.PasteboardType, pasteboard: NSPasteboard) -> NSPasteboard.ReadingOptions
  {
    return .asString
  }

	required convenience init?(pasteboardPropertyList propertyList: Any, ofType type: NSPasteboard.PasteboardType)
  {
    guard let someURL = NSURL(pasteboardPropertyList:propertyList, ofType:type) as URL? else { return nil }
    self.init(fileLocation:someURL, modificationDate:nil, fileSize:0)
  }

  //MARK: NSSecureCoding

  func encode(with coder:NSCoder)
  {
    coder.encode(fileName as NSString, forKey:"file name")
    if let modificationDate = lastModified
    {
      coder.encode(modificationDate as NSDate, forKey:"last modified")
    }
    coder.encode(self.location as NSURL, forKey:"location")
    coder.encode(self.sizeInBytes, forKey:"size")
  }

  required init?(coder decoder:NSCoder)
  {
    sizeInBytes = UInt(decoder.decodeInteger(forKey: "size"))
		guard let decodedLocation = decoder.decodeObject(of: NSURL.self, forKey:"location") else { return nil }
    location = decodedLocation as URL
    fileName = (decoder.decodeObject(of:NSString.self, forKey:"file name") as String?) ?? ""
    lastModified = decoder.decodeObject(of:NSDate.self, forKey:"last modified") as Date?
  }

  static var supportsSecureCoding : Bool { true }

  //MARK: NSCopying

	func copy(with zone: NSZone?) -> Any
  {
    FXFileBrowserFileData(fileLocation:self.location, modificationDate:self.lastModified, fileSize:self.sizeInBytes)
  }

}

//MARK: Operator Overloading

func < (lhs: FXFileBrowserFileData, rhs: FXFileBrowserFileData) -> Bool
{
  lhs.compare(rhs) == .orderedAscending
}

func > (left: FXFileBrowserFileData, rhs: FXFileBrowserFileData) -> Bool
{
  left.compare(rhs) == .orderedDescending
}
