//
//  FXSyntaxHighlighter.h
//  FXKit
//
//  Copyright (c) 2013-2021 Kai Özer. All rights reserved.
//

@protocol FXSyntaxHighlighter <NSObject>

@property (nonatomic, retain) NSFont* font;

- (NSAttributedString*) highlight:(NSString*)input;

@end
