//
//  FXClipView.swift
//  FXKit
//
//  Created by Kai Özer on 7/21/14.
//  Copyright (c) 2014-2021 Kai Özer. All rights reserved.
//

import Cocoa

@objc public class FXClipView : NSClipView
{
  /// For enabling or disabling constrainment of the document view to a minimum size.
  /// You should turn constraining off if you use constraints based AppKit layout.
  @objc public var constrainsDocumentSize : Bool = true

  @objc public var minDocumentViewHeight : CGFloat = 0.0
  @objc public var minDocumentViewWidth : CGFloat = 0.0

  // This offset is needed when the clip view is used inside of an NSScrollView.
  // It helps make the vertical scrollbar disappear.
  // If no offset is used then the height of the document view becomes stuck
  // at a few pixels (determined by NSScrollView and NSClipView) larger than
  // the content view, thus the scrollbar does not disappear.
  @objc public var verticalClipOffset : CGFloat = 0.0

  private var _flippedClipView : Bool

  @objc public init(frame:NSRect, flipped:Bool)
  {
    _flippedClipView = flipped
    super.init(frame: frame)
  }

  public required init?(coder:NSCoder)
  {
    _flippedClipView = false
    super.init(coder:coder)
  }

  public convenience override init(frame:NSRect)
  {
    self.init(frame:frame, flipped:false)
  }

  //MARK: NSClipView overrides

  public override var documentView : NSView?
  {
    get { return super.documentView }
    set {
      super.documentView = newValue
      if let newDocView = newValue
      {
        newDocView.autoresizingMask = .none
      }
    }
  }

  //MARK: NSView overrides

  public override func setFrameSize(_ newSize: NSSize)
  {
    if ( constrainsDocumentSize )
    {
      var newDocumentSize = newSize
      newDocumentSize.width = ( newSize.width < self.minDocumentViewWidth ) ? self.minDocumentViewWidth : newSize.width;
      newDocumentSize.height = ( newSize.height < (self.minDocumentViewHeight + self.verticalClipOffset) ) ? self.minDocumentViewHeight : (newSize.height - self.verticalClipOffset);
      if let view = documentView
      {
        view.frame = NSMakeRect(0,0,round(newDocumentSize.width),round(newDocumentSize.height))
      }
    }
    super.setFrameSize(newSize)
  }

  //MARK: NSResponder overrides

  public override func encodeRestorableState(with coder:NSCoder)
  {
    super.encodeRestorableState(with: coder)
    documentView?.encodeRestorableState(with: coder)
  }

  public override func restoreState(with coder:NSCoder)
  {
    super.restoreState(with: coder)
    documentView?.restoreState(with: coder)
  }

}
