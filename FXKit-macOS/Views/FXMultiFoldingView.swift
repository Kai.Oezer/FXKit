//
//  FXMultiFoldingView.swift
//  FXKit
//
//  Created by Kai Özer on 7/24/14.
//  Copyright (c) 2014, 2021 Kai Özer. All rights reserved.
//
import Cocoa


@objc public class FXMultiFoldingView : NSView
{
  private var _splitter : NSSplitView
  private var _foldingSubviews = [FXMultiFoldingSubview]()

  @objc public override init(frame:NSRect)
  {
    _splitter = NSSplitView(frame:frame)
    super.init(frame:frame)
    _splitter.dividerStyle = .paneSplitter
    _splitter.delegate = self
    _splitter.isVertical = false
    super.addSubview(_splitter)
    FXViewUtils.layout(in: self, visualFormats:["H:|[mSplitter]|", "V:|[mSplitter]|"], metricsInfo:nil, viewsInfo:["mSplitter":_splitter])
  }

	@objc public required convenience init?(coder:NSCoder)
  {
    self.init(frame: .zero)
  }

  @objc public func addSubview(_ view : NSView, title : String)
  {
    let foldingSubview = FXMultiFoldingSubview(title:title)
    _splitter.addSubview(foldingSubview)
    foldingSubview.contentView = view
    FXViewUtils.layout(in: _splitter,
      visualFormats:["V:[foldingSubview(>=minHeight)]"],
      metricsInfo:["minHeight":foldingSubview.minDocumentViewHeight],
      viewsInfo:["foldingSubview":foldingSubview])
    _foldingSubviews.append(foldingSubview)
  }

  private func _handleFoldingAction()
  {
//		DispatchQueue.main.async {
//
//		}
  }

  private func _newFoldingSubview(title : String) -> FXMultiFoldingSubview
  {
    let foldingBox = FXMultiFoldingSubview(title: title)
    foldingBox.action = _handleFoldingAction
    return foldingBox
  }

  //MARK: mark NSResponder overrides

  private let FXResume_SplitterPositions = "FXResume_SplitterPositions"

  public override func restoreState(with coder:NSCoder)
  {
    super.restoreState(with: coder)
    guard let subviewHeights = coder.decodeObject(forKey: FXResume_SplitterPositions) as? [CGFloat] else { return }
    _splitter.subviews.forEach({ $0.restoreState(with:coder) })
    _splitter.adjustSubviews()
    var currentPos : CGFloat = 0.0
    for (index, height) in subviewHeights.enumerated()
    {
      if index < (self._splitter.subviews.count - 1)
      {
        currentPos += height + (CGFloat(index) * self._splitter.dividerThickness)
        _splitter.setPosition(currentPos, ofDividerAt: index)
      }
    }
    self.needsDisplay = true
  }

  public override func encodeRestorableState(with coder:NSCoder)
  {
    super.encodeRestorableState(with: coder)
    var subviewHeights = [CGFloat]()
    for subview in _splitter.subviews
    {
			subview.encodeRestorableState(with: coder)
			subviewHeights.append(subview.frame.size.height)
    }
    coder.encode(subviewHeights, forKey:FXResume_SplitterPositions)
  }

}

 extension FXMultiFoldingView : NSSplitViewDelegate
{
  public func splitView(_ splitView:NSSplitView, canCollapseSubview subview:NSView) -> Bool
  {
    return true
  }

  public func splitView(_ splitView:NSSplitView, shouldCollapseSubview subview:NSView, forDoubleClickOnDividerAt dividerIndex:NSInteger) -> Bool
  {
    return true
  }
}


// MARK: - MultiFoldingSubview

let skFoldingSubviewHeaderHeight : CGFloat = 18.0


private class FXMultiFoldingSubview : FXClipView
{
  private var _triangleButton : NSButton?
  private var _titleField : NSTextField
  private var _minSize = CGSize.zero
  private var _containerView : FXClipView

  var action : (()->())?

  var isFolded : Bool { _triangleButton?.state == .off }

  var title : String
  {
    get { _titleField.stringValue }
    set { _titleField.stringValue = newValue }
  }

  init(title:String)
  {
    let dummyFrame = CGRect(x:0, y:0, width:200, height:200)

    _titleField = NSTextField(frame:dummyFrame)
    _titleField.stringValue = title
    _titleField.isBordered = false
    _titleField.backgroundColor = NSColor(deviceRed:0.4, green:0.42, blue:0.4, alpha:1)
    _titleField.drawsBackground = true
    _titleField.textColor = NSColor(deviceWhite:0.9, alpha:1.0)
    _titleField.font = NSFont(name:"Lucida Grande", size:12.0)
    _titleField.isSelectable = false
    _titleField.isEditable = false
    _titleField.focusRingType = .none
    _titleField.alignment = .center

    _containerView = FXClipView(frame:dummyFrame)

    let view = NSView(frame:dummyFrame)
    view.subviews = [_titleField, _containerView]

    FXViewUtils.layout(in: view,
      visualFormats:["H:|[title]|", "H:|[container]|", "V:|[title(titleHeight)][container(>=32)]|"],
      metricsInfo:["titleHeight" : skFoldingSubviewHeaderHeight],
      viewsInfo:["container" : _containerView, "title" : _titleField]);

    super.init(frame:dummyFrame, flipped:false)
    self.title = title
    self.documentView = view
    self.minDocumentViewHeight = skFoldingSubviewHeaderHeight;
  }

	required convenience init?(coder : NSCoder)
  {
		let title = (coder.decodeObject(forKey: "Title") as? String) ?? ""
    self.init(title: title)
  }

  var contentView : NSView?
  {
    set {
      _containerView.documentView = newValue
      _containerView.removeConstraints(_containerView.constraints)
      if let view = newValue
      {
        FXViewUtils.layout(in:_containerView, visualFormats:["H:|[view]|", "V:|[view]|"], metricsInfo:nil, viewsInfo:["view":view])
        self.minDocumentViewHeight = skFoldingSubviewHeaderHeight + view.fittingSize.height
      }
    }
    get {
      return _containerView.documentView
    }
  }

  func handleDisclosureAction(sender : AnyObject)
  {
    assert(sender === _triangleButton, "Action sent from unknown control.")
    self.action?()
  }

  override func encodeRestorableState(with coder:NSCoder)
  {
    super.encodeRestorableState(with: coder)
    _containerView.encodeRestorableState(with: coder)
  }

  override func restoreState(with coder:NSCoder)
  {
    super.restoreState(with: coder)
    _containerView.restoreState(with: coder)
  }
}

