//
//  MenuPopupView.swift
//  Volta
//
//  Created by Kai Özer on 7/20/14.
//  Copyright (c) 2014, 2017 Kai Özer. All rights reserved.
//

import Cocoa

@objc public class MenuPopupView : NSView
{
  var image : NSImage? = nil
  var menuOffset : NSPoint = NSZeroPoint

  override init(frame:NSRect)
  {
    super.init(frame:frame)
  }

  public required init?(coder:NSCoder)
  {
    super.init(coder:coder)
  }

  func selectItemWithTitle(title:String)
  {
    if let menu = self.menu
    {
      for item in menu.items
      {
        item.state = (item.title == title) ? .on : .off
      }
    }
  }

  //MARK: NSView overrides

  override public func draw(_ dirtyRect : NSRect)
  {
    if let image = self.image
    {
      let imageSize = image.size
      let viewSize = self.frame.size
      let imageRect = NSMakeRect((viewSize.width - imageSize.width)/2, (viewSize.height - imageSize.height)/2, imageSize.width, imageSize.height)
      image.draw(in: imageRect)
    }
  }


  override public var intrinsicContentSize : NSSize
  {
    if let image = self.image {
      return image.size
    }
    return NSMakeSize(NSView.noIntrinsicMetric, NSView.noIntrinsicMetric)
  }


  override public func mouseDown(with event: NSEvent)
  {
    let mouseLocation = convert(event.locationInWindow, from:nil)
    if NSPointInRect(mouseLocation, self.bounds)
    {
      if let menu = self.menu
      {
        menu.popUp(positioning: nil, at:self.menuOffset, in:self)
      }
    }
  }


  override public func acceptsFirstMouse(for event:NSEvent?) -> Bool
  {
    return true
  }
}
