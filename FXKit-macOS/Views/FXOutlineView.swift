//
//  FXOutlineView.swift
//  FXKit
//
//  Created by Kai Özer on 7/21/14.
//  Copyright (c) 2014-2021 Kai Özer. All rights reserved.
//
import Cocoa

/// This customization of NSOutlineView provides enhancements like
/// - sending a notification when the delete key is pressed,
/// - collapsing/expanding items when double clicked on,
/// - ability to set a custom image for dragging
public class FXOutlineView : NSOutlineView
{
  /// Sent when the delete key is pressed on the outline view.
  /// Notification object: The FXOutlineView instance that sends the notification
  public static let DeleteKeyNotification = "FXOutlineViewDeleteKeyNotification"

  @objc var dragImage : NSImage?

  public override func keyDown(with keyEvent: NSEvent)
  {
    if let key = keyEvent.charactersIgnoringModifiers?.unicodeScalars.first
    {
      if key == Unicode.Scalar(NSDeleteCharacter) || key == Unicode.Scalar(NSBackspaceCharacter)
      {
        NotificationCenter.default.post(name:NSNotification.Name(rawValue:FXOutlineView.DeleteKeyNotification), object: self)
        return
      }
    }
    super.keyDown(with: keyEvent)
  }

  public override func dragImageForRows(with dragRows: IndexSet, tableColumns: [NSTableColumn], event dragEvent: NSEvent, offset dragImageOffset: NSPointPointer) -> NSImage
  {
    if self.dragImage != nil
    {
      return self.dragImage!
    }
    return super.dragImageForRows(with: dragRows, tableColumns: tableColumns, event: dragEvent, offset: dragImageOffset)
  }
}
