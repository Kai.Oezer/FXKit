//
//  FXOverlayTextLineView.swift
//  FXKit
//
//  Created by Kai Özer on 7/20/14.
//  Copyright (c) 2014, 2017 Kai Özer. All rights reserved.
//

import Cocoa


@objc public class FXOverlayTextLineView : NSView
{
  @objc public var text = ""
  @objc public var showsFlippedText = false

  @objc public override var isOpaque: Bool
  {
    return false
  }

	static private let kTextAttributes : [NSAttributedString.Key : AnyObject]? = [
		.font : NSFont(name:"Lucida Grande", size:18.0)!,
		.foregroundColor : NSColor(deviceWhite:0.8, alpha:0.7)
	]

  @objc public override func draw(_ dirtyRect : NSRect)
  {
    let textSize = text.size(withAttributes: Self.kTextAttributes)
    let textPosition = CGPoint(x: 0.5 * (dirtyRect.size.width - textSize.width), y:(showsFlippedText ? 0.4 : 0.6) * dirtyRect.size.height)
    text.draw(at: textPosition, withAttributes: Self.kTextAttributes)
  }
}
