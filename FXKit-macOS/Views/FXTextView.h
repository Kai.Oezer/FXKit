//
//  FXTextView.h
//  FXKit
//
//  Copyright (c) 2007-2021 Kai Özer. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@class FXTextView;
@protocol FXSyntaxHighlighter;


@protocol FXTextViewDelegate <NSObject>
- (void) textWillChange:(FXTextView*)textView;
- (void) textDidChange:(FXTextView*)textView;
@end


/// A text view with support for line numbering
@interface FXTextView : NSView < NSCoding, NSTextViewDelegate >

@property (nullable, weak) id<FXTextViewDelegate> delegate;
@property (nonatomic, copy) NSString* string;
@property (assign, nonatomic) NSFont* font;
@property (nonatomic, copy) NSColor* gutterBackgroundColor;
@property (nonatomic, copy) NSColor* gutterSeparatorColor;
@property (nonatomic, copy) NSColor* gutterTextColor;
@property (nonatomic) float gutterWidth;
@property (nullable, nonatomic) id<FXSyntaxHighlighter> syntaxHighlighter;

+ (FXTextView*) newPrintableInstance;

@end

NS_ASSUME_NONNULL_END
