//
//  FXKit.h
//  FXKit-macOS
//
//  Created by Kai Özer on 8/10/14.
//  Copyright (c) 2014, 2017 Kai Özer. All rights reserved.
//

#import <AppKit/AppKit.h>

FOUNDATION_EXPORT double FXKitVersionNumber;
FOUNDATION_EXPORT const unsigned char FXKitVersionString[];

#import <FXKit/FXKit-Common.h>

// headers of Objective-C classes for macOS only
#import <FXKit/FXSyntaxHighlighter.h>
#import <FXKit/FXTextView.h>
