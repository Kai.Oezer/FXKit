/**
This file is part of the Volta project.
Copyright (C) 2007-2013 Kai Berk Oezer
https://robo.fish/wiki/index.php?title=Volta
https://gitlab.com/Kai.Oezer/Volta

Volta is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#import <XCTest/XCTest.h>
#import <FXKit/FXString.hpp>
#include <iostream>

@interface Test_FXString : XCTestCase
@end


@implementation Test_FXString

- (void) test_basics
{
  FXString s;
  XCTAssert(s.empty());
}

- (void) test_tokenize
{
  FXStringVector slist = FXString("m 11 12 L 21 22").tokenize();
  XCTAssert( slist.size() == 6 );
  XCTAssert( slist[0] == "m" );
  XCTAssert( slist[1] == "11" );
  XCTAssert( slist[2] == "12" );
  XCTAssert( slist[3] == "L" );
  XCTAssert( slist[4] == "21" );
  XCTAssert( slist[5] == "22" );
  FXStringVector slist2 = FXString("aba\tdoing\tgar").tokenize("\t");
  XCTAssert( slist2.size() == 3 );
}

- (void) test_upper_lower_case
{
  XCTAssert( FXString("bNaRguuG").upperCase() == FXString("BNARGUUG") );
  XCTAssert( FXString("HaRnnMuD").lowerCase() == FXString("harnnmud") );
}

- (void) test_suffix_prefix
{
  XCTAssert( FXString("GingerBread").startsWith("Ginger") );
  XCTAssert( FXString("SommerRain").endsWith("Rain") );
}

- (void) test_trim_and_crop
{
  XCTAssert( FXString("  hello ").trim() == "hello" );
  XCTAssert( FXString("@brr!").trim("@") == "brr!" );
  XCTAssert( FXString("@brr!").trim("!") == "@brr" );
  XCTAssert( FXString("	 tabAndSpace	  ").trimWhitespace() == "tabAndSpace" );
  XCTAssert( FXString("tatalolotattr").crop(4,4) == "lolo" );
}

- (void) test_value_extraction
{
  XCTAssert( FXString("1.35").extractFloat() == 1.35f );
  XCTAssert( FXString("true").extractBoolean() );
  XCTAssert( !FXString("false").extractBoolean() );
  XCTAssert( FXString("12").extractLong() == 12 );
  XCTAssert( FXString("-23.000006").extractDouble() == -23.000006 );
}

- (void) test_search
{
  FXString s("My name is Bond. What's your name?");
  XCTAssert( s.find("name") == 3 );
  XCTAssert( s.find("name", 10) == 29 );
  XCTAssert( s.find("James") < 0 );
}

- (void) test_string_extraction
{
  FXString s("The length() function returns the number of elements in the current string");
  try
  {
    s.substring(s.length() + 5);
    XCTAssert(NO);
  }
  catch (std::runtime_error& e)
  {
    XCTAssert(YES);
  }

  XCTAssert(s.substring(13, 4) == "func");
  XCTAssert(s.substring(60) == "current string");
  XCTAssert(s.substring(0,10) == "The length");
}

- (void) test_getlines
{
  const char * cstr ="\nbla bal\rhelp\n\nbingo\r\n\f\rjames\rbond\n\r";
  const char * cstr2 ="\nbla bal\rhelp\n\nbingo\r\n\f\rjames\rbond\n\rhallo";
  FXString str(cstr);
  FXString str2(cstr2);
  FXStringVector list = str.getLines(true);
  FXStringVector list2 = str2.getLines(true);
  FXStringVector listWithEmptyLines = str.getLines(false);
  XCTAssert(list.size() == 5);
  XCTAssert(list2.size() == 6);
  XCTAssert(listWithEmptyLines.size() == 11);
  XCTAssert(list.at(0) == "bla bal");
  XCTAssert(list.at(1) == "help");
  XCTAssert(list.at(2) == "bingo");
  XCTAssert(list.at(3) == "james");
  XCTAssert(list.at(4) == "bond");
  XCTAssert(list2.at(5) == "hallo");
  XCTAssert(listWithEmptyLines.at(0).empty());
  XCTAssert(listWithEmptyLines.at(1) == "bla bal");
  XCTAssert(listWithEmptyLines.at(2) == "help");
  XCTAssert(listWithEmptyLines.at(3).empty());
  XCTAssert(listWithEmptyLines.at(4) == "bingo");
  XCTAssert(listWithEmptyLines.at(5).empty());
  XCTAssert(listWithEmptyLines.at(6).empty());
  XCTAssert(listWithEmptyLines.at(7).empty());
  XCTAssert(listWithEmptyLines.at(8) == "james");
  XCTAssert(listWithEmptyLines.at(9) == "bond");
  XCTAssert(listWithEmptyLines.at(10).empty());
}

- (void) test_unicode
{
  FXString s1("künçe");
  std::unique_ptr<char[]> charArray = s1.cString();
  XCTAssert( strlen(charArray.get()) == 7 );
  XCTAssert( s1.substring(1,1) == FXString("ü") );
}

- (void) test_unique_ptr_with_arrays
{
  static int const kNumTestClassInstance = 3;
  static int FXStringTestClassACount = 0;
  struct FXStringTestClassA
  {
    FXStringTestClassA()  { FXStringTestClassACount++; }
    ~FXStringTestClassA() { FXStringTestClassACount--; }
  };

  {
    std::unique_ptr<FXStringTestClassA[]> arrayOfInstances( new FXStringTestClassA[kNumTestClassInstance] );
    XCTAssertEqual( FXStringTestClassACount, kNumTestClassInstance );
  }
  XCTAssertEqual( FXStringTestClassACount, 0 );
}

@end
