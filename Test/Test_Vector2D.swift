//
//  Vector2DTest.swift
//  FXKit
//
//  Created by Kai Özer on 2/3/15.
//  Copyright (c) 2015, 2017 Kai Özer. All rights reserved.
//

import Foundation
import XCTest
@testable import FXKit

let Epsilon = 1e-10

class Test_Vector2D : XCTestCase
{
  func test_addition()
  {
    let v1 = Vector2D(3.2, 4.5)
    let v2 = Vector2D(-1.0, 3)
    let v3 = v1 + v2
		XCTAssertEqual(v3.x, 2.2, accuracy: Epsilon, "vector addition is not correct")
		XCTAssertEqual(v3.y, 7.5, accuracy: Epsilon, "vector addition is not correct")
  }

  func test_division()
  {
    var v = Vector2D(4.0,6.0)
    v /= 2.0
		XCTAssertEqual(v.x, 2.0, accuracy: Epsilon, "vector division is not correct")
		XCTAssertEqual(v.y, 3.0, accuracy: Epsilon, "vector division is not correct")
  }

  func test_dot_product()
  {
    let v1 = Vector2D(5.0, 4.0)
    let v2 = Vector2D(1.0, -1.5)
    let dot = v1.dot(v2)
		XCTAssertEqual(dot, -1.0, accuracy: Epsilon, "vector dot product is not correct")
  }
}
