//
//  Test_ColorUtils.swift
//  FXKit
//
//  Created by Kai Özer on 4/23/15.
//  Copyright (c) 2015, 2017 Kai Özer. All rights reserved.
//

import XCTest
@testable import FXKit

class Test_ColorUtils : XCTestCase
{
  func test_color_from_string()
  {
    if let color = cgColorFromHexString("0x00FF00"), let components = color.components
    {
      XCTAssertEqual(components[0], 0.0, "red channel should be 0")
      XCTAssertEqual(components[1], 1.0, "green channel should be 1.0")
      XCTAssertEqual(components[2], 0.0, "blue channel should be 0")
    }
    else
    {
      XCTAssert(false, "There should be a valid CGColor instance.")
    }
  }

#if os(iOS)
  func test_string_from_color()
  {
    let color = UIColor(red:0.5, green:0.0, blue: 1.0, alpha: 1.0).cgColor
    let hexString = hexStringFromCGColor(color)
    XCTAssertEqual(hexString, "0x8000ff", "the string does not match the expected value")
    let color2 = UIColor(red: 0.8, green: 0.1, blue: 0.2, alpha: 0.5).cgColor
    let hexString2 = hexStringFromCGColor(color2)
    XCTAssertEqual(hexString2, "0xcc1a33", "the string does not match the expected value")
  }
#endif
}
