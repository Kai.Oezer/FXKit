//
//  Test_FontUtils.swift
//  FXKit Tests
//
//  Created by Kai Özer on 4/29/15.
//  Copyright (c) 2015, 2017 Kai Özer. All rights reserved.
//

import XCTest
@testable import FXKit

class Test_FontUtils : XCTestCase
{
  func test_descriptionFromFont()
  {
    if let font = UIFont(name:"Helvetica", size: 24.0)
    {
      let description = FXFontUtils.descriptionFromFont(font)
      XCTAssertEqual(description, "Helvetica#24.0")
    }
    else
    {
      XCTAssert(false)
    }
  }

  func test_fontFromDescription()
  {
    if let font = FXFontUtils.fontFromDescription("Arial-BoldMT#18.0")
    {
      XCTAssertEqual(font.fontName, "Arial-BoldMT")
      XCTAssertEqual(font.pointSize, 18.0)
    }
    else
    {
      XCTAssert(false)
    }
  }
}

