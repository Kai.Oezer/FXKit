/**
This file is part of the FXKit project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXKit

FXKit is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import XCTest
@testable import FXKit

class Test_XMLElement : XCTestCase
{
	func testAddRemoveChildren()
	{
		let element = FXXMLElement(name: "parent")
		XCTAssertEqual( element.childCount, 0 )
		let child1 = FXXMLElement(name: "child1")
		element.addChild( child1 )
		XCTAssertEqual( element.childCount, 1 )
		let child2 = FXXMLElement(name: "child2")
		element.addChild( child2 )
		XCTAssertEqual( element.childCount, 2 )
		XCTAssert( element.hasChild(child2) )
		element.removeChild(child2)
		XCTAssert( !element.hasChild(child2) )
		XCTAssertEqual( element.childCount, 1 )
		element.removeChild(child1)
		XCTAssertEqual( element.childCount, 0 )
	}

	func testDepthCalculation()
	{
		let root = FXXMLElement(name: "root")
		let child_1 = FXXMLElement(name: "child_1")
		let child_1_1 = FXXMLElement(name: "child_1_1")
		let child_1_1_1 = FXXMLElement(name: "child_1_1_1")
		let child_2 = FXXMLElement(name: "child_2")
		let child_2_1 = FXXMLElement(name: "child_2_1")
		let child_2_2 = FXXMLElement(name: "child_2_2")
		let child_2_2_1 = FXXMLElement(name: "child_2_2_1")
		let child_2_2_1_1 = FXXMLElement(name: "child_2_2_1_1")

		root.addChild(child_1)
		XCTAssertEqual( root.maxDepth, 1 )
		child_1.addChild(child_1_1)
		XCTAssertEqual( root.maxDepth, 2 )
		child_1_1.addChild(child_1_1_1)
		XCTAssertEqual( root.maxDepth, 3 )
		root.addChild(child_2)
		XCTAssertEqual( root.maxDepth, 3 )
		child_2.addChild(child_2_1)
		XCTAssertEqual( root.maxDepth, 3 )
		child_2.addChild(child_2_2)
		XCTAssertEqual( root.maxDepth, 3 )
		child_2_1.addChild(child_2_2_1)
		XCTAssertEqual( root.maxDepth, 3 )
		child_2_2_1.addChild(child_2_2_1_1)
		XCTAssertEqual( root.maxDepth, 4 )
	}

	func testPrintTree()
	{
		let root = FXXMLElement(name: "root")
		let child_1 = FXXMLElement(name:"child_1")
		let child_1_1 = FXXMLElement(name:"child_1_1")
		let child_1_2 = FXXMLElement(name:"child_1_2")
		let child_2 = FXXMLElement(name:"child_2")
		child_2.attributes["x"] = "5"
		let child_2_1 = FXXMLElement(name: "child_2_1")
		let child_2_1_1 = FXXMLElement(name: "child_2_1_1")
		root.addChild(child_1)
		root.addChild(child_2)
		child_1.addChild(child_1_1)
		child_1.addChild(child_1_2)
		child_2.addChild(child_2_1)
		child_2_1.addChild(child_2_1_1)

		XCTAssertEqual(child_2.printTree(), "child_2 [x=5]\n  child_2_1\n    child_2_1_1\n")
		XCTAssertEqual(root.printTree("+-", "+-"), "+-root\n+-+-child_1\n+-+-+-child_1_1\n+-+-+-child_1_2\n+-+-child_2 [x=5]\n+-+-+-child_2_1\n+-+-+-+-child_2_1_1\n")
	}

	func testTreeCopy()
	{
		let root = FXXMLElement(name: "root")
		let child_1 = FXXMLElement(name:"child_1")
		let child_2 = FXXMLElement(name:"child_2")
		let child_2_1 = FXXMLElement(name:"child_2_1")
		root.addChild(child_1)
		root.addChild(child_2)
		child_2.addChild(child_2_1)

		guard let rootCopy = root.copyTree() else { XCTFail("could not copy root element"); return }
		XCTAssert( rootCopy !== root )
		XCTAssertEqual( rootCopy.maxDepth, root.maxDepth )
    XCTAssertEqual( rootCopy.childCount, root.childCount )
    XCTAssertEqual( root.childCount, 2 )
    XCTAssert( rootCopy.child(at:0) !== root.child(at:0) )
    XCTAssert( rootCopy.child(at:1) !== root.child(at:1) )
    XCTAssertEqual( rootCopy.child(at:1)!.childCount, 1 )
    XCTAssert( rootCopy.child(at:1)!.child(at:0)! !== root.child(at:1)!.child(at:0) )

    XCTAssertEqual( rootCopy.printTree(), root.printTree() )

		let filter : (FXXMLElement)->(FXXMLElement?) = {
			$0.attributes["att"] = "bla"
			return $0
		}
    guard let filteredCopy = root.copyTree(filter:filter) else { XCTFail("could not filter root element"); return }
    XCTAssertEqual( filteredCopy.maxDepth, root.maxDepth )
    XCTAssert( filteredCopy !== root )
    XCTAssertEqual( filteredCopy.printTree(), "root [att=bla]\n  child_1 [att=bla]\n  child_2 [att=bla]\n    child_2_1 [att=bla]\n" )
	}

	func testCollectingChildElementsByName()
	{
		let root = FXXMLElement(name: "root")
		let child_1 = FXXMLElement(name: "coffee")
		let child_1_1 = FXXMLElement(name: "cocoa")
		let child_1_2 = FXXMLElement(name: "coffee")
		let child_2 = FXXMLElement(name: "coffee")
		let child_2_1 = FXXMLElement(name: "cocoa")
		let child_2_1_1 = FXXMLElement(name: "coffee")
		root.addChild(child_1)
		root.addChild(child_2)
		child_1.addChild(child_1_1)
		child_1.addChild(child_1_2)
		child_2.addChild(child_2_1)
		child_2_1.addChild(child_2_1_1)

		let coffees = root.childrenWithName("coffee")
		XCTAssertEqual( coffees.count, 2 )
		let allCoffees = root.childrenWithName("coffee", recursive: true)
		XCTAssertEqual( allCoffees.count, 4 )
		let cocoas = root.childrenWithName("cocoa")
		XCTAssert( cocoas.isEmpty )
		let allCocoas = root.childrenWithName("cocoa", recursive: true)
		XCTAssertEqual( allCocoas.count, 2 )
	}
}
