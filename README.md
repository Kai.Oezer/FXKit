![FXKit](fxkit_logo.png)

# FXKit

This repository contains a framework with custom views and some utility functions for use in iOS and macOS app development. I created it in 2014 by factoring out re-usable components from [Volta](https://github.com/robo-fish/Volta). In 2016, I updated the code for Swift 3 and compatibility with the latest iOS/macOS APIs. In 2021, I updated for Swift 5 and iOS 14 / macOS 11 compatibility.

Some of the code here may still be useful for today's app developers. Mostly, however, it's example code for how we did GUIs before SwiftUI. Moreover, the framework format is not practical anymore for distributing open-source utility libraries. Swift Packages are easier to set up, easier to distribute, and leave smaller binary footprints in the finished application. So, if you find something useful for you in this framework, I encourage you to create a Swift package for it.

