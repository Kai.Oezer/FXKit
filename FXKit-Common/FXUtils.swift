//
//  FXUtils.swift
//  FXKit
//
//  Copyright (c) 2014 Kai Özer. All rights reserved.
//

import Foundation

// https://stackoverflow.com/questions/33294620/how-to-cast-self-to-unsafemutablepointervoid-type-in-swift/
func bridge<T : AnyObject>(obj : T) -> UnsafeMutableRawPointer {
  return UnsafeMutableRawPointer(Unmanaged.passUnretained(obj).toOpaque())
}

func bridge<T : AnyObject>(ptr : UnsafeRawPointer) -> T {
  return Unmanaged<T>.fromOpaque(ptr).takeUnretainedValue()
}

class FXUtils
{

}

func FXLocString(_ key: String) -> String
{
  return Bundle(for: FXUtils.self).localizedString(forKey: key, value: "!translate!", table: "FXKit")
}
