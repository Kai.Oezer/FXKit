/**
This file is part of the Volta project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXKit

FXKit is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import Foundation
import libxml2

/// Creates and manages a DOM tree from a given XML file or content string.
/// The tree contains only nodes of type element. Other node types, like
/// comments, are skipped during parsing.
public class FXXMLDocument
{
	public let rootElement : FXXMLElement

	public init(rootElement : FXXMLElement)
	{
		self.rootElement = rootElement
	}

  /// Constructs itself from parsing the XML document at the given location.
	public static func from(documentLocation : URL) -> FXXMLDocument?
	{
		guard let filePath = documentLocation.path.cString(using: .utf8) else { return nil }
		guard let reader = xmlReaderForFile(filePath, xmlGetCharEncodingName(XML_CHAR_ENCODING_UTF8), Int32(XML_PARSE_NOWARNING.rawValue)) else { return nil }
		defer { xmlFreeTextReader(reader) }
		return Self._coreReader(reader: reader)
	}

	public static func from(documentContent : String) -> FXXMLDocument?
	{
		guard !documentContent.isEmpty else { return nil }
		guard let contentCStr = documentContent.cString(using: .utf8) else { return nil }
		guard let reader = xmlReaderForMemory(contentCStr, Int32(strlen(contentCStr)), "", xmlGetCharEncodingName(XML_CHAR_ENCODING_UTF8), Int32(XML_PARSE_NOWARNING.rawValue)) else { return nil }
		defer { xmlFreeTextReader(reader) }
		return Self._coreReader(reader: reader)
	}

	/// - returns: `true` if the given document is valid against the given Relax NG schema,
	/// `false` if the document is not valid or an error occurred during validation.
	public static func validate(documentContent : String, ngSchema : String) -> Bool
	{
		guard let documentCharacters = documentContent.cString(using: .utf8) else { return false }
		guard let schemaCharacters = ngSchema.cString(using: .utf8) else { return false }

		guard let reader = xmlReaderForMemory(documentCharacters, Int32(strlen(documentCharacters)), "", xmlGetCharEncodingName(XML_CHAR_ENCODING_UTF8), Int32(XML_PARSE_NOWARNING.rawValue)) else {
			debugPrint("Could not create XML reader for given document content.")
			return false
		}
		defer { xmlFreeTextReader(reader) }

		guard let rngParserContext = xmlRelaxNGNewMemParserCtxt(schemaCharacters, Int32(strlen(schemaCharacters))) else {
			debugPrint("Could not create Relax NG parser context.")
			return false
		}
		defer { xmlRelaxNGFreeParserCtxt(rngParserContext) }

		// setting the error handler callback
		//xmlRelaxNGSetParserErrors(rngParserContext, validityErrorHandler, validityWarningHandler, nil)

		guard let rngRef = xmlRelaxNGParse(rngParserContext) else {
			debugPrint("Error while parsing the Relax NG validation schema.")
			return false
		}
		defer { xmlRelaxNGFree(rngRef) }

		guard xmlTextReaderRelaxNGSetSchema(reader, rngRef) != -1 else {
			debugPrint("Error while trying to associate the Relax NG validation schema with the document.")
			return false
		}

		var ret : Int32 = 0
		repeat { ret = xmlTextReaderRead(reader) } while ret == 1
		guard ret != -1 else {
			debugPrint("Could not read from document while trying to validate.")
			return false
		}

		return xmlTextReaderIsValid(reader) == 1
	}

	private static func _coreReader(reader : xmlTextReaderPtr) -> FXXMLDocument?
	{
		var elementStack = [FXXMLElement]()
		var lastDepth : Int32 = -1
		var ret : Int32 = 0
		var lastElement : FXXMLElement?
		var documentRoot : FXXMLElement?

    //xmlCheckVersion(20904) // check that the libxml runtime version is compatible with the build-time version

		ret = xmlTextReaderRead(reader)
		while ret == 1
		{
			guard let name_ = xmlTextReaderConstLocalName(reader) else { break }

			let name = String(cString: name_)
			let nodeType = xmlTextReaderNodeType(reader)
			if nodeType == XML_READER_TYPE_ELEMENT.rawValue
			{
        // This XML item is an element. Add to it to the tree structure.
        let depth = xmlTextReaderDepth(reader)

        let element = FXXMLElement(name: name)

        if lastElement == nil
        {
          documentRoot = element
        }

        if depth > lastDepth
        {
          // Put reference to parent on stack
          if let lastElement = lastElement
          {
            // The last element becomes the new parent
            elementStack.append( lastElement )
          }
        }
        else if depth < lastDepth
        {
					elementStack.removeLast(Int(lastDepth - depth))
        }

        lastElement = element
        lastDepth = depth

        // Add this element to parent, if it exists
        if !elementStack.isEmpty
        {
					elementStack.last?.addChild(element)
        }

        if (xmlTextReaderHasAttributes(reader) != 0)
					&& (xmlTextReaderMoveToFirstAttribute(reader) > 0)
				{
					let attributeName = String(cString: xmlTextReaderConstLocalName(reader))
					let attributeValue = String(cString: xmlTextReaderConstValue(reader))
					element.attributes[attributeName] = attributeValue
					while xmlTextReaderMoveToNextAttribute(reader) == 1
					{
						let attrName = String(cString: xmlTextReaderConstLocalName(reader))
						let attrValue = String(cString: xmlTextReaderConstValue(reader))
						element.attributes[attrName] = attrValue
					}
				}
			}
			else if nodeType == XML_READER_TYPE_TEXT.rawValue
			{
				if let lastElement = lastElement,
					xmlTextReaderHasValue(reader) != 0,
					let value = xmlTextReaderConstValue(reader)
				{
					lastElement.value = String(cString: value)
				}
			}
			ret = xmlTextReaderRead(reader)
		}

		guard ret == 0, let documentRoot = documentRoot else { return nil }
		return FXXMLDocument(rootElement: documentRoot)
	}
}

extension FXXMLDocument : CustomStringConvertible
{
	public var description : String
	{
		rootElement.printTree()
	}
}
