//
//  FXFontUtils.swift
//  FXKit
//
//  Created by Kai Özer on 4/29/15.
//  Copyright (c) 2015, 2017 Kai Özer. All rights reserved.
//

#if os(iOS)

public class FXFontUtils
{

  public class func fontFromDescription( _ description : String ) -> UIFont?
  {
    var result : UIFont?
    let tokens = description.split(separator:"#", maxSplits:Int.max, omittingEmptySubsequences: true).map { String($0) }
    if tokens.count == 2
    {
      result = UIFont(name: tokens[0], size: CGFloat((tokens[1] as NSString).floatValue))
    }
    return result
  }


  public class func descriptionFromFont( _ font : UIFont ) -> String
  {
    return String(format:"%@#%.1f", font.fontName, font.pointSize)
  }

}

#endif
