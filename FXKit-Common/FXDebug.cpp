//
//  FXDebug.cpp
//  FXKit
//
//  Copyright (c) 2013-2021 Kai Özer. All rights reserved.
//

#import "FXDebug.h"
#include <pthread.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/sysctl.h>
#include <execinfo.h> // for stack trace
#include <iostream>


void FXDebug::interrupt()
{
  pthread_kill( pthread_self(), SIGINT );
}


bool FXDebug::amIBeingDebugged()
{
  // See Apple Technical Q&A document QA1361: http://developer.apple.com/library/mac/#qa/qa1361/_index.html

  struct kinfo_proc info;
  info.kp_proc.p_flag = 0;

  // Initializing mib to query the information about a specific process ID.
  int mib[4] = { CTL_KERN, KERN_PROC, KERN_PROC_PID, getpid() };

  size_t size = sizeof(info);
  int junk = sysctl(mib, sizeof(mib) / sizeof(*mib), &info, &size, NULL, 0);
  assert(junk == 0);

  return ( (info.kp_proc.p_flag & P_TRACED) != 0 );
}


void FXDebug::printStackTrace()
{
  static const int skMaxTraces = 30;
  void* traceBuffer[skMaxTraces];
  int numTraces = backtrace((void**)(&traceBuffer), skMaxTraces);
  char** symbols = backtrace_symbols((void**)(&traceBuffer), numTraces);
  for ( int i = 0; i < numTraces; i++ )
  {
    std::cerr << symbols[i] << std::endl;
  }
}
