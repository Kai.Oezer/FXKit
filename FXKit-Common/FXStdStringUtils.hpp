//
//  FXStdStringUtils.hpp
//  FXKit
//
//  Copyright (c) 2007-2021 Kai Özer. All rights reserved.
//

#pragma once

#if defined(__cplusplus)

#include <vector>
#include <string>

using namespace std;

class FXStdStringUtils
{
public:
  static string upperCase(string const & input);

  static string lowerCase(string const & input);

  static vector<string> tokenize( string const & input, string const & separator, bool includeSeparators );

  static vector<string> getLines( string const & input, bool ignoreEmptyLines );

  static string trimWhitespace( string const & input );

  static string extension( string const & input );

  static string crop( string const & input, size_t startIndex, size_t cropLength );

  static void trim( string & input, char const toBeRemoved );

  static void replaceAll( string & input, string const & to_be_replaced, string const & replacement );
};

#endif // __cplusplus
