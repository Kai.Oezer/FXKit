//
//  FXKit-Common.h
//  FXKit
//
//  Created by Kai Özer on 8/10/14.
//  Copyright (c) 2014-2021 Kai Özer. All rights reserved.
//

// Objective-C headers need to be included here instead of a bridging header.

#pragma once

#import <Foundation/Foundation.h>

//FOUNDATION_EXPORT double FXKitVersionNumber;
//FOUNDATION_EXPORT const unsigned char FXKitVersionString[];

#import <FXKit/FXKitDefines.h>
#import <FXKit/FXNetworkConnectionChecker.h>
#import <FXKit/FXSystemUtils.h>
#import <FXKit/FXDebug.h>
#import <FXKit/FXStdStringUtils.hpp>
#import <FXKit/FXString.hpp>

#if 0
// FOR CLEAN BUILDS, COMMENT THE NEXT #IMPORT LINE OUT.
// This auto-generated header provides access to Swift classes from Objective-C.
// This header will not exist at the beginning of a clean build
// After the first build, the Swift classes will have been compiled and
// this header will have been generated.
// THEN UNCOMMENT THE LINE AND BUILD AGAIN.
// This method is not suitable for automated builds in continuous integration environments.
#import <FXKit/FXKit-Swift.h>
#else
// Objective-C code, which wants to use the Swift classes of this framework need
// to import the Swift interface explicitly, as shown below:
//
//    #import <FXKit/FXKit-Swift.h>
//
// A solution for making this duality managable at the client code side, is to create
// a precompiled header which includes both <FXKit/FXKit.h> and <FXKit/FXKit-Swift.h>.
#endif
