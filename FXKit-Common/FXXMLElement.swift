/**
This file is part of the FXKit project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXKit

FXKit is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import Foundation

public class FXXMLElement
{
	public var name : String
	public var value : String
	public var attributes = [String : String]()

	private var children = [FXXMLElement]()

	public init(name : String)
	{
		self.name = name
		value = ""
	}

	public func addChild(_ child : FXXMLElement)
	{
		if !children.contains(where:{ $0 === child })
		{
			children.append(child)
		}
	}

	public func removeChild(_ child : FXXMLElement)
	{
		children.removeAll{ $0 === child }
	}

	public var childCount : Int { children.count }

	public func hasChild(_ child : FXXMLElement) -> Bool
	{
		children.contains{ $0 === child }
	}

	public func child(at index : Int) -> FXXMLElement?
	{
		children[index]
	}

	public func childrenWithName(_ name : String, recursive : Bool = false) -> [FXXMLElement]
	{
		var result = [FXXMLElement]()
		for child in children
		{
			if child.name == name
			{
				result.append(child)
			}
			if recursive
			{
				result.append(contentsOf: child.childrenWithName(name, recursive: recursive))
			}
		}
		return result
	}

	public func copyTree(filter : ((FXXMLElement)->(FXXMLElement?))? = nil) -> FXXMLElement?
	{
		let copy = FXXMLElement(name: self.name)
		copy.value = self.value
		copy.attributes = self.attributes
		copy.children = self.children.compactMap{ $0.copyTree(filter: filter) }
		guard let filter = filter else { return copy }
		return filter(copy)
	}

	public func printTree(_ prefix : String = "", _ indentationString : String = "  ") -> String
	{
		prefix + self.description + "\n" + children.map { $0.printTree(prefix + indentationString, indentationString) }.joined()
	}

	public var maxDepth : Int
	{
		guard !children.isEmpty else { return 0 }
		var maxChildDepth = 0
		for child in children
		{
			maxChildDepth = max(maxChildDepth, child.maxDepth)
		}
		return maxChildDepth + 1
	}
}

extension FXXMLElement : CustomStringConvertible
{
	public var description : String
	{
		"\(name)" + (attributes.isEmpty ? "" : " [\(attributes.map{"\($0.key)=\($0.value)"}.joined(separator: " "))]")
	}
}
