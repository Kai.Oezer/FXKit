/**
This file is part of the FXKit project.
Copyright (C) 2021 Kai Berk Oezer
https://gitlab.com/Kai.Oezer/FXKit

FXKit is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

import Foundation

/// Simple abstraction type for complex numbers.
///
/// For more advanced complex number support see the `Complex` type in
/// the [Swift Numerics](https://github.com/apple/swift-numerics) package.
public struct FXComplexNumber<F> : Codable where F : FloatingPoint & Codable
{
	var real : F
	var imaginary : F

	var magnitude : F { sqrt(real * real + imaginary * imaginary) }

	init(_ real : F, _ imaginary : F)
	{
		self.real = real
		self.imaginary = imaginary
	}

	enum ComplexNumberCodingKey : CodingKey
	{
		case real
		case imaginary
	}

	public init(from decoder: Decoder) throws
	{
		let container = try decoder.container(keyedBy: ComplexNumberCodingKey.self)
		self.real = try container.decode(F.self, forKey: .real)
		self.imaginary = try container.decode(F.self, forKey: .imaginary)
	}

	public func encode(to encoder: Encoder) throws
	{
		var container = encoder.container(keyedBy: ComplexNumberCodingKey.self)
		try container.encode(self.real, forKey: .real)
		try container.encode(self.imaginary, forKey: .imaginary)
	}
}

extension FXComplexNumber : Equatable
{
	public static func == <T : FloatingPoint>(lhs : FXComplexNumber<T>, rhs : FXComplexNumber<T>) -> Bool
	{
		lhs.real.isEqual(to: rhs.real) && lhs.imaginary.isEqual(to: rhs.imaginary)
	}
}

extension FXComplexNumber : AdditiveArithmetic
{
	public static func - (lhs: FXComplexNumber<F>, rhs: FXComplexNumber<F>) -> FXComplexNumber<F>
	{
		FXComplexNumber(lhs.real - rhs.real, lhs.imaginary - rhs.imaginary)
	}

	public static func + (lhs: FXComplexNumber<F>, rhs: FXComplexNumber<F>) -> FXComplexNumber<F>
	{
		FXComplexNumber(lhs.real + rhs.real, lhs.imaginary + rhs.imaginary)
	}

	public static var zero: FXComplexNumber<F>
	{
		FXComplexNumber(F.zero, F.zero)
	}
}

extension FXComplexNumber : CustomStringConvertible
{
	public var description : String { "(\(real),\(imaginary))" }
}
