//
//  FXSystemUtils.mm
//  FXKit
//
//  Created by Kai Özer on 10/12/14.
//  Copyright (c) 2014, 2017 Kai Özer. All rights reserved.
//

#import "FXSystemUtils.h"
#include <sys/sysctl.h>
#include <mach/machine.h>
#if TARGET_OS_OSX
#include <AppKit/NSWorkspace.h>
#endif

@implementation FXSystemUtils

+ (NSString*) cpuArchitecture
{
  cpu_type_t type;
  cpu_subtype_t subtype;
  size_t size = sizeof(type);
  sysctlbyname("hw.cputype", &type, &size, NULL, 0);
  size = sizeof(subtype);
  sysctlbyname("hw.cpusubtype", &subtype, &size, NULL, 0);

  // values for cputype and cpusubtype defined in mach/machine.h
  if (type == CPU_TYPE_X86)
  {
    return (subtype == CPU_SUBTYPE_X86_64_ALL) || (subtype == CPU_SUBTYPE_X86_64_H) ? @"x86_64" : @"x86";
  }
  else if (type == CPU_TYPE_ARM)
  {
    switch (subtype)
    {
      case CPU_SUBTYPE_ARM_V7:
      case CPU_SUBTYPE_ARM_V7F: return @"armv7";
      case CPU_SUBTYPE_ARM_V7S: return @"armv7s";
    }
  }
  else if (type == CPU_TYPE_ARM64)
  {
    return @"arm64";
  }

  return nil;
}

+ (NSURL*) appSupportFolder
{
	NSArray<NSURL*> * urls = [NSFileManager.defaultManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask];
	if ( [urls count] > 0 )
	{
		NSAssert( [urls count] == 1, @"There must be one, and only one, application support folder." );
		return urls[0];
	}
	return nil;
}


+ (NSURL*) appSupportAlternativeFolderWithName:(NSString *)folderName
{
	NSArray<NSURL*> * urls = [NSFileManager.defaultManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask];
	if ( [urls count] > 0 )
	{
		NSAssert( [urls count] == 1, @"There must be one, and only one, application support folder." );
		return [urls[0] URLByAppendingPathComponent:folderName];
	}
	return nil;
}


+ (void) removeContentsOfDirectoryAtLocation:(NSURL*)directoryLocation
{
	NSFileManager* fm = [NSFileManager defaultManager];
	BOOL isDir = NO;
	if ( [fm fileExistsAtPath:[directoryLocation path] isDirectory:&isDir] && isDir )
	{
		NSArray* directoryContents = [fm contentsOfDirectoryAtURL:directoryLocation includingPropertiesForKeys:nil options:0 error:NULL];
		if ( directoryContents != nil )
		{
			for ( NSURL* fileLocation in directoryContents )
			{
				[fm removeItemAtURL:fileLocation error:NULL];
			}
		}
	}
}


+ (BOOL) copyContentsOfDirectory:(NSURL*)sourceDirectory
										 toDirectory:(NSURL*)targetDirectory
											replaceAll:(BOOL)replace
							 overwriteExisting:(BOOL)overwrite
													 error:(NSError**)fileError
{
	NSFileManager* fm = [NSFileManager defaultManager];
	BOOL result = NO;
	BOOL isDir = NO;
	if ( [fm fileExistsAtPath:[sourceDirectory path] isDirectory:&isDir] && isDir )
	{
		isDir = NO;
		if ( [fm fileExistsAtPath:[targetDirectory path] isDirectory:&isDir] && isDir )
		{
			result = YES;
			if ( replace )
			{
				[self removeContentsOfDirectoryAtLocation:targetDirectory];
			}
			NSArray* sourceFiles = [fm contentsOfDirectoryAtURL:sourceDirectory includingPropertiesForKeys:nil options:0 error:NULL];
			if ( sourceFiles != nil )
			{
				for ( NSURL* sourceFileLocation in sourceFiles )
				{
					NSURL* targetFileLocation = [targetDirectory URLByAppendingPathComponent:[sourceFileLocation lastPathComponent]];
					if ( [fm fileExistsAtPath:[targetFileLocation path]] )
					{
						if ( overwrite )
						{
							if ( ![fm removeItemAtURL:targetFileLocation error:fileError] )
							{
								result = NO;
								break;
							}
							if (![fm copyItemAtURL:sourceFileLocation toURL:targetFileLocation error:fileError])
							{
								result = NO;
								break;
							}
						}
						else
						{
							continue;
						}
					}
					else
					{
						if (![fm copyItemAtURL:sourceFileLocation toURL:targetFileLocation error:fileError])
						{
							result = NO;
							break;
						}
					}
				}
			}
		}
	}
	return result;
}


+ (NSURL*) localDocumentsFolderLocation
{
	NSArray* userDocumentDirectories = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
	if ( (userDocumentDirectories != nil) && (userDocumentDirectories.count > 0) )
	{
		return userDocumentDirectories[0];
	}
	return nil;
}


+ (void) crash
{
	*((char*)0x08) = 1;
}

#if TARGET_OS_OSX

+ (void) revealFileAtLocation:(NSURL*)location
{
	if (location == nil)
		return;

#if 1
	[[NSWorkspace sharedWorkspace] selectFile:[location path] inFileViewerRootedAtPath:@""];
#else
	// OS X 10.7 incorrectly sets the quarantine attribute for folders created inside the own sandbox.
	// The quarantine attribute prevents [NSWorkspace openURL:] from opening folders in Finder.
	// The problem does not occur in OS X 10.8. The solution is to use "/usr/bin/open" with the "R" option.
	// An additional advantage of using 'open -R <file_path>' is that Finder highlights the file.
	// See radar:12143159
	NSTask* task = [[NSTask alloc] init];
	task.launchPath = @"/usr/bin/open";
	task.arguments = @[@"-R", [location path]];
	[task launch];
#endif
}

static void folderChangeHandler(
	ConstFSEventStreamRef streamRef,
	void* clientCallBackInfo,
	size_t numEvents,
	void* eventPaths,
	const FSEventStreamEventFlags eventFlags[],
	const FSEventStreamEventId eventIds[])
{
	@autoreleasepool
	{
		id observer = (__bridge id)clientCallBackInfo;
		if ([observer conformsToProtocol:@protocol(FXFolderChangeObserver)])
		{
			[(id<FXFolderChangeObserver>)observer handleFolderContentsChange];
		}
	}
}

+ (void) setUpFolderMonitoringForLocation:(NSURL*)location eventStream:(FSEventStreamRef)eventStream observer:(id<FXFolderChangeObserver>)observer
{
	if ( eventStream != NULL )
	{
		FSEventStreamStop(eventStream);
		FSEventStreamInvalidate(eventStream);
		FSEventStreamRelease(eventStream);
	}
	if ( location != nil )
	{
		CFStringRef cfFolderPath = (__bridge CFStringRef)[location path];
		CFArrayRef monitoredFolders = CFArrayCreate(kCFAllocatorDefault, (const void **)&cfFolderPath, 1, &kCFTypeArrayCallBacks);
		CFTimeInterval const eventDelay = 0.1;
		FSEventStreamCreateFlags const creationFlags = kFSEventStreamCreateFlagNone;
		FSEventStreamContext* streamContext = (FSEventStreamContext*) calloc(1, sizeof(FSEventStreamContext)); // FSEventStreamCreate crashes in Release build if the context is not a heap variable
		streamContext->info = (__bridge void*)observer;
		eventStream = FSEventStreamCreate(NULL, folderChangeHandler, streamContext, monitoredFolders, kFSEventStreamEventIdSinceNow, eventDelay, creationFlags);
		CFRelease(monitoredFolders);
		FSEventStreamScheduleWithRunLoop(eventStream, CFRunLoopGetCurrent(), kCFRunLoopDefaultMode);
		FSEventStreamStart(eventStream);
		free(streamContext);
	}
}

#endif // TARGET_OS_OSX

@end
