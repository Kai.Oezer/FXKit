//
//  FXSystemUtils.h
//  FXKit
//
//  Created by Kai Özer on 10/12/14.
//  Copyright (c) 2014-2021 Kai Özer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

#if TARGET_OS_OSX
@protocol FXFolderChangeObserver
- (void) handleFolderContentsChange;
@end
#endif // TARGET_OS_OSX


@interface FXSystemUtils : NSObject

+ (NSString*) cpuArchitecture;

+ (nullable NSURL*) appSupportFolder;

/// @return the location of a sub-folder with the given name that lies in the user's generic Application Support folder.
+ (nullable NSURL*) appSupportAlternativeFolderWithName:(NSString*)folderName;

+ (void) removeContentsOfDirectoryAtLocation:(NSURL*)directoryLocation;

/// @return YES if the files could be copied successfully
/// @param replace whether to replace (delete) all files in the target directory
/// @param overwrite whether to overwrite files in the target directory with the same name as files in the source directory
+ (BOOL) copyContentsOfDirectory:(NSURL*)sourceDirectory
                     toDirectory:(NSURL*)targetDirectory
                      replaceAll:(BOOL)replace
               overwriteExisting:(BOOL)overwrite
                           error:(NSError**)fileError;

+ (nullable NSURL*) localDocumentsFolderLocation;

// Causes the app to crash through a Bad Access operation
+ (void) crash;

#if TARGET_OS_OSX

+ (void) revealFileAtLocation:(NSURL*)location;


// Notifies of file system changes
+ (void) setUpFolderMonitoringForLocation:(NSURL*)location
                              eventStream:(FSEventStreamRef)eventStream
                                 observer:(id<FXFolderChangeObserver>)observer;

#endif // TARGET_OS_OSX

@end

NS_ASSUME_NONNULL_END
