//
//  FXNetworkConnectionChecker.h
//  FXKit
//
//  Created by Kai Özer on 8/2/14.
//  Copyright (c) 2014-2021 Kai Özer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^ConnectionCheckHandler)(NSString* checkedAddress, BOOL isReachable);


/// Asynchronously checks the network connection
@interface FXNetworkConnectionChecker : NSObject

/// @return YES if the asynchronous check has been started successfully, otherwise NO.
- (BOOL) checkConnectionToHost:(NSString*)hostAddress handler:(ConnectionCheckHandler)checkHandler;

/// @return YES if the asynchronous check has been started successfully, otherwise NO.
- (BOOL) checkConnectionToInternetWithHandler:(ConnectionCheckHandler)checkHandler;

/// @return YES if the asynchronous check has been started successfully, otherwise NO.
- (BOOL) checkConnectionToWiFiWithHandler:(ConnectionCheckHandler)checkHandler;

/// @return YES if successfully stopped
- (BOOL) stopChecking;

@end

NS_ASSUME_NONNULL_END
