//
//  FXViewUtils.swift
//  FXKit
//
//  Created by Kai Özer on 7/25/14.
//  Copyright (c) 2014-2021 Kai Özer. All rights reserved.
//

#if os(OSX)
import Cocoa
#else
import UIKit
#endif

@objc public class FXViewUtils : NSObject
{
#if os(OSX)
  /// - returns: an image that represents a snapshot of the given view.
  @objc public class func image(of view : NSView) -> NSImage?
  {
    let bounds = view.bounds
    guard let bitmapImageRep : NSBitmapImageRep = view.bitmapImageRepForCachingDisplay(in: bounds) else { return nil }
		bzero(bitmapImageRep.bitmapData, bitmapImageRep.bytesPerRow * bitmapImageRep.pixelsHigh)
		view.cacheDisplay(in: bounds, to:bitmapImageRep)
		let image = NSImage(size:bitmapImageRep.size)
		image.addRepresentation(bitmapImageRep)
    return image;
  }
#else
  // Use this method if you want to turn a view into an image and then process the image before using it.
  @objc public class func image(of view : UIView) -> UIImage
  {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, false, 0.0)
    view.drawHierarchy(in: view.bounds, afterScreenUpdates:false)
    let result = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return result!
  }

  // Use this method if you want to turn a view into an image (embedded in a view) that will not be further processed.
  @objc public class func snapshotViewFromView(_ view:UIView) -> UIView
  {
    return view.snapshotView(afterScreenUpdates: false)!
  }
#endif

  /**
      Convenience method for adding layout constraints to a view.

      Adds layout constraints of subviews to the given view according to the given visual format strings, the given metric names dictionary and the given view names dictionary.
  
      - parameter view: the view to which the constraints are added
  */
  @objc public class func layout(in view:FXView,
    visualFormats : [String],
    metricsInfo : [String:CGFloat]?,
    viewsInfo : [String:FXView])
  {
    viewsInfo.values.forEach { $0.translatesAutoresizingMaskIntoConstraints = false }
    for formatString in visualFormats
    {
      view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: formatString, options:[], metrics:metricsInfo, views:viewsInfo))
    }
  }

  @objc public static func transferSubviews(from sourceView : FXView, to targetView : FXView)
  {
    let views = sourceView.subviews
    sourceView.subviews.forEach{ $0.removeFromSuperview() }
    views.forEach{ targetView.addSubview($0) }
    FXViewUtils._recursiveTransferConstraints(from:sourceView, to:targetView, for:targetView)
  }

  private static func _recursiveTransferConstraints(from sourceView : FXView, to targetView : FXView, for currentView : FXView)
  {
    for c in currentView.constraints
    {
      if let newFirstItem = (c.firstItem === sourceView) ? targetView : c.firstItem,
        let newSecondItem = (c.secondItem === sourceView) ? targetView : c.secondItem
      {
        targetView.addConstraint(NSLayoutConstraint(item: newFirstItem, attribute: c.firstAttribute, relatedBy: c.relation, toItem: newSecondItem, attribute: c.secondAttribute, multiplier: c.multiplier, constant: c.constant))
      }
    }
    for subview in currentView.subviews
    {
      _recursiveTransferConstraints(from:sourceView, to:targetView, for:subview)
    }
  }

}

public extension FXView
{
  func layoutWithVisualFormats(_ visualFormats:[String], metricsInfo metrics:[String:CGFloat]!, viewsInfo views:[String:FXView]!)
  {
    FXViewUtils.layout(in: self, visualFormats:visualFormats, metricsInfo:metrics, viewsInfo:views)
  }
}
