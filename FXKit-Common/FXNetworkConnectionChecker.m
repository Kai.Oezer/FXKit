//
//  FXNetworkConnectionChecker.m
//  FXKit
//
//  Created by Kai Özer on 8/2/14.
//  Copyright (c) 2014-2021 Kai Özer. All rights reserved.
//

#import "FXNetworkConnectionChecker.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <arpa/inet.h>

@interface FXNetworkConnectionChecker ()
- (void) _notifyThatTheHostIsReachable:(BOOL)reachable;
@end


static void reachabilityCallback(SCNetworkReachabilityRef target, SCNetworkReachabilityFlags flags, void* info)
{
  if (info != NULL)
  {
    if ([(__bridge NSObject*)info isKindOfClass:[FXNetworkConnectionChecker class]])
    {
      FXNetworkConnectionChecker* checker = (__bridge FXNetworkConnectionChecker*)info;
      BOOL canConnect = (flags & kSCNetworkReachabilityFlagsReachable)
      || (flags & kSCNetworkReachabilityFlagsTransientConnection)
    #if TARGET_OS_IPHONE
      || (flags & kSCNetworkReachabilityFlagsIsWWAN)
    #endif
      || (flags & kSCNetworkReachabilityFlagsConnectionRequired)
      || (flags & kSCNetworkReachabilityFlagsConnectionOnTraffic);
      [checker _notifyThatTheHostIsReachable:canConnect];
    }
  }
}


@implementation FXNetworkConnectionChecker
{
@private
  ConnectionCheckHandler handler;
  NSString* checkedHost;
  SCNetworkReachabilityRef reachability;
  CFRunLoopRef runloop;
}

- (void) dealloc
{
  CFRelease(reachability);
}

// MARK: Public


- (BOOL) checkConnectionToHost:(NSString*)hostAddress handler:(ConnectionCheckHandler)checkHandler
{
  if (hostAddress != nil)
  {
    checkedHost = [hostAddress copy];
    handler = checkHandler;
    reachability = SCNetworkReachabilityCreateWithName(NULL, [checkedHost UTF8String]);
    return [self _startCheckOfReachability];
  }
  return NO;
}


- (BOOL) checkConnectionToInternetWithHandler:(ConnectionCheckHandler)checkHandler
{
  struct sockaddr_in zeroAddress;
  bzero(&zeroAddress, sizeof(zeroAddress));
  zeroAddress.sin_len = sizeof(zeroAddress);
  zeroAddress.sin_family = AF_INET;
  
  return [self _checkConnectionToAdress:&zeroAddress handler:checkHandler];
}


- (BOOL) checkConnectionToWiFiWithHandler:(ConnectionCheckHandler)checkHandler
{
  struct sockaddr_in localWifiAddress;
  bzero(&localWifiAddress, sizeof(localWifiAddress));
  localWifiAddress.sin_len = sizeof(localWifiAddress);
  localWifiAddress.sin_family = AF_INET;

  // IN_LINKLOCALNETNUM is defined in <netinet/in.h> as 169.254.0.0.
  localWifiAddress.sin_addr.s_addr = htonl(IN_LINKLOCALNETNUM);

  return [self _checkConnectionToAdress:&localWifiAddress handler:checkHandler];
}


- (BOOL) stopChecking
{
  BOOL result = NO;
  if (reachability != NULL)
  {
    if (runloop != NULL)
    {
      result = SCNetworkReachabilityUnscheduleFromRunLoop(reachability, runloop, kCFRunLoopDefaultMode);
      CFRelease(reachability);
      reachability = NULL;
    }
  }
  return result;
}


// MARK: Private


- (BOOL) _checkConnectionToAdress:(const struct sockaddr_in *)hostAddress handler:(ConnectionCheckHandler)checkHandler
{
  checkedHost = nil;
  handler = checkHandler;
  reachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (const struct sockaddr *)hostAddress);
  return [self _startCheckOfReachability];
}


- (void) _notifyThatTheHostIsReachable:(BOOL)isReachable
{
  handler(checkedHost, isReachable);
}


- (BOOL) _startCheckOfReachability
{
  if (reachability != NULL)
  {
    SCNetworkReachabilityContext context = {0, (__bridge void *)(self), NULL, NULL, NULL};

    if (SCNetworkReachabilitySetCallback(reachability, reachabilityCallback, &context))
    {
      runloop = CFRunLoopGetCurrent();
      if (SCNetworkReachabilityScheduleWithRunLoop(reachability, runloop, kCFRunLoopDefaultMode))
      {
        return YES;
      }
    }
  }
  return NO;
}


@end
