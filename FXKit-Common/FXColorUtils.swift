//
//  FXColorUtils.swift
//  FXKit
//
//  Created by Kai Özer on 8/14/14.
//  Copyright (c) 2014-2021 Kai Özer. All rights reserved.
//

#if os(iOS)

import UIKit

public extension UIColor
{
  /// - returns: A brighter or darker variant of the color without changing the hue or saturation.
  func intensified(_ factor:CGFloat) -> UIColor
  {
    var hue : CGFloat = 0.0
    var sat : CGFloat = 0.0
    var brt : CGFloat = 0.0
    var alf : CGFloat = 1.0
    if self.getHue(&hue, saturation:&sat, brightness:&brt, alpha:&alf)
    {
      return UIColor(hue:hue, saturation:sat, brightness: factor * brt, alpha: alf)
    }
    return self
  }

  /// - returns: A lighter or darker variant, depending on the given factor.
  /// - parameter factor: needs to be greater than 1 in order to produce a lighter color
  func lighter(_ factor:CGFloat) -> UIColor
  {
    var h : CGFloat = 0.0
    var s : CGFloat = 0.0
    var b : CGFloat = 0.0
    var a : CGFloat = 1.0
    if self.getHue(&h, saturation: &s, brightness: &b, alpha: &a)
    {
      return UIColor(hue: h, saturation: s, brightness: max(0.0, min(1.0, factor*b)), alpha:a)
    }
    return self
  }
}

#endif

public func hexStringFromCGColor(_ cgColor : CGColor) -> String
{
  let components = cgColor.__unsafeComponents
  let red = _hexStringFromFloat((components?[0])!)
  let green = _hexStringFromFloat((components?[1])!)
  let blue = _hexStringFromFloat((components?[2])!)
  return "0x" + red + green + blue
}

public func cgColorFromHexString(_ hexString : String) -> CGColor?
{
  if let hex = _intFromHexString(hexString)
  {
    let red =   CGFloat((hex & 0xFF0000) >> 16)
    let green = CGFloat((hex & 0x00FF00) >> 8)
    let blue =  CGFloat((hex & 0x0000FF))
    var components : [CGFloat] = [red, green, blue, 1.0]
    return CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: &components)
  }
  return nil
}

/// - returns: -1 if the given string does not represent a hexadecimal value
private func _intFromHexString(_ hex : String) -> UInt32?
{
  let scanner = Scanner(string:hex)
  scanner.charactersToBeSkipped = CharacterSet(charactersIn:"#")
  guard let int32value = scanner.scanInt32(representation: .hexadecimal) else { return nil }
	return UInt32(int32value)
}

private func _hexStringFromFloat(_ f : CGFloat) -> String
{
  let intValue = Int(round(f * 255.0))
  let stringValue = String(intValue, radix:16, uppercase:false)
  return intValue < 16 ? "0" + stringValue : stringValue
}
