//
//  Vector2D.swift
//  FXKit
//
//  Created by Kai Özer on 2/2/15.
//  Copyright (c) 2015, 2017 Kai Özer. All rights reserved.
//

import CoreGraphics
//import cmath

let Epsilon = 1e-20

public struct Vector2D
{
  public var x : Double
  public var y : Double

  public init(_ x : Double = 0.0, _ y : Double = 0.0)
  {
    self.x = x
    self.y = y
  }

  public init(point : CGPoint)
  {
    x = Double(point.x)
    y = Double(point.y)
  }

  public func magnitude() -> Double
  {
    return sqrt(x*x + y*y)
  }

  public func squaredMagnitude() -> Double
  {
    return x*x + y*y
  }

  //! Turns the vector into a unit vector
  public mutating func normalize()
  {
    let mag = magnitude()
    if ( mag > 0.0 )
    {
      x /= mag
      y /= mag
    }
    else
    {
      x = 1.0
      y = 0.0
    }
  }

  //! \return a normalized copy
  public func asUnitVector() -> Vector2D
  {
    var v = Vector2D(x,y)
    v.normalize()
    return v
  }

  //! \return an inverted copy
  public func inverse() -> Vector2D
  {
    return Vector2D(-x, -y)
  }

  //! inverts this vector
  public mutating func invert()
  {
    x = -x
    y = -y
  }

  public func dot(_ v : Vector2D) -> Double
  {
    return x * v.x + y * v.y
  }

  //! \return the angle between two vectors, in radian
  public func angle(_ v : Vector2D) -> Double
  {
    return acos( dot(v) / v.magnitude() / magnitude() )
  }

  //! Rotates the vector by the given angle around the (optional) pivot point
  //! \param angle counterclockwise rotation angle in radians
  //! \param pivot pivot point
  public mutating func rotate( _ angle : Double, pivot : Vector2D = Vector2D(0.0, 0.0) )
  {
    let cos_a = cos(angle)
    let sin_a = sin(angle)
    x -= pivot.x
    y -= pivot.y
    let xrot = (x * cos_a) - (y * sin_a)
    let yrot = (x * sin_a) + (y * cos_a)
    x = xrot + pivot.x
    y = yrot + pivot.y
  }

  public mutating func scale( _ scale_x : Double, scale_y : Double )
  {
    x *= scale_x
    y *= scale_y
  }

}

public func == (v1 : Vector2D, v2 : Vector2D) -> Bool
{
  return (fabs(v1.x - v2.x) < Epsilon) && (fabs(v1.y - v2.y) < Epsilon)
}

public func != (v1 : Vector2D, v2 : Vector2D) -> Bool
{
  return (fabs(v1.x - v2.x) >= Epsilon) || (fabs(v1.y - v2.y) >= Epsilon)
}

public func < (v1 : Vector2D, v2 : Vector2D) -> Bool
{
  return (v1.x == v2.x) ? (v1.y < v2.y) : (v1.x < v2.x);
}

public func + (v1 : Vector2D, v2 : Vector2D) -> Vector2D
{
  return Vector2D(v1.x + v2.x, v1.y + v2.y)
}

public func - (v1 : Vector2D, v2 : Vector2D) -> Vector2D
{
  return Vector2D(v1.x - v2.x, v1.y - v2.y)
}

public func * (v : Vector2D, f : Double) -> Vector2D
{
  return Vector2D(f * v.x, f * v.y)
}

public func * (f : Double, v : Vector2D) -> Vector2D
{
  return Vector2D(f * v.x, f * v.y)
}

public func / (v : Vector2D, f : Double) -> Vector2D
{
  return Vector2D(v.x/f, v.y/f)
}

public func += (v1 : inout Vector2D, v2 : Vector2D)
{
  v1.x += v2.x;
  v1.y += v2.y;
}

public func -= (v1 : inout Vector2D, v2 : Vector2D)
{
  v1.x -= v2.x
  v1.y -= v2.y
}

public func *= (v : inout Vector2D, f : Double)
{
  v.x *= f
  v.y *= f
}

public func /= (v : inout Vector2D, f : Double)
{
  v.x /= f
  v.y /= f
}

