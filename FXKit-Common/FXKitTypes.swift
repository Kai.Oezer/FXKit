//
//  FXKitTypes.swift
//  FXKit
//
//  Copyright (c) 2014-2024 Kai Özer. All rights reserved.
//

#if os(iOS)
import UIKit
#else
import Cocoa
#endif


#if os(iOS)

  public let       FXDrawingToScreen    = { ()->Bool in return true }
  public typealias FXView               = UIView
  public typealias FXScrollView         = UIScrollView
  public typealias FXViewController     = UIViewController
  public typealias FXTextField          = UITextField
  public typealias FXEvent              = UIEvent
  public typealias FXImage              = UIImage
  public typealias FXFont               = UIFont
  public typealias FXColor              = UIColor

  public let FXViewHeightSizable  = UIView.AutoresizingMask.flexibleHeight
  public let FXViewWidthSizable   = UIView.AutoresizingMask.flexibleWidth
  public let FXViewMinXMargin     = UIView.AutoresizingMask.flexibleLeftMargin
  public let FXViewMaxXMargin     = UIView.AutoresizingMask.flexibleRightMargin
  public let FXViewMinYMargin     = UIView.AutoresizingMask.flexibleBottomMargin
  public let FXViewMaxYMargin     = UIView.AutoresizingMask.flexibleTopMargin

  public func FXGraphicsContext() -> CGContext? { return UIGraphicsGetCurrentContext() }
  public func FXSetNeedsDisplay(_ x:UIView) { x.setNeedsDisplay() }
  public func FXMaxX(_ r:CGRect) -> CGFloat { return r.maxX }
  public func FXMaxY(_ r:CGRect) -> CGFloat { return r.maxY }
  public func FXMinX(_ r:CGRect) -> CGFloat { return r.minX }
  public func FXMinY(_ r:CGRect) -> CGFloat { return r.minY }
  public func FXPointInRect(_ p : CGPoint, _ r:CGRect) -> Bool { return r.contains(p) }

#else

  public typealias FXView               = NSView
  public typealias FXScrollView         = NSScrollView
  public typealias FXViewController     = NSViewController
  public typealias FXTextField          = NSTextField
  public typealias FXEvent              = NSEvent
  public typealias FXImage              = NSImage
  public typealias FXFont               = NSFont
  public typealias FXColor              = NSColor

  public let FXViewHeightSizable  = NSView.AutoresizingMask.height
  public let FXViewWidthSizable   = NSView.AutoresizingMask.width
  public let FXViewMinXMargin     = NSView.AutoresizingMask.minXMargin
  public let FXViewMaxXMargin     = NSView.AutoresizingMask.maxXMargin
  public let FXViewMinYMargin     = NSView.AutoresizingMask.minYMargin
  public let FXViewMaxYMargin     = NSView.AutoresizingMask.maxYMargin

  public func FXSetNeedsDisplay(x:NSView) { x.needsDisplay = true }
  public func FXMaxX(r : NSRect) -> CGFloat { return NSMaxX(r) }
  public func FXMaxY(r : NSRect) -> CGFloat { return NSMaxY(r) }
  public func FXMinX(r : NSRect) -> CGFloat { return NSMinX(r) }
  public func FXMinY(r : NSRect) -> CGFloat { return NSMinY(r) }
  public func FXPointInRect(p : NSPoint, _ r : NSRect) -> Bool { return NSPointInRect(p, r) }

  public func FXGraphicsContext() -> CGContext
  {
    NSGraphicsContext.current!.cgContext
  }

  public func FXDrawingToScreen() -> Bool { return NSGraphicsContext.currentContextDrawingToScreen() }

#endif


#if arch(x86_64) || arch(arm64)

  /// Note: On 64-Bit architecture, in Objective-C, NSPoint equals CGPoint, NSRect equals CGRect, NSSize equals CGSize

  public typealias FXSize               = CGSize
  public let       FXSizeZero           = CGSize.zero
  public typealias FXPoint              = CGPoint
  public let       FXPointZero          = CGPoint.zero
  public typealias FXRect               = CGRect
  public let       FXRectZero           = CGRect.zero

  public func FXSizeMake(_ width:CGFloat, _ height:CGFloat) -> CGSize { return CGSize(width: width, height: height) }
  public func FXRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect { return CGRect(x: x, y: y, width: width, height: height) }
  public func FXPointMake(_ x: CGFloat, _ y: CGFloat) -> CGPoint { return CGPoint(x: x, y: y) }
//  public typealias FXRectMake           = CGRectMake
//  public typealias FXEqualRects         = CGRectEqualToRect
//  public typealias FXInsetRect          = CGRectInset
//  public typealias FXUnionRect          = CGRectUnion
//  public typealias FXMaxX               = CGRectGetMaxX
//  public typealias FXMaxY               = CGRectGetMaxY
//  public typealias FXMinX               = CGRectGetMinX
//  public typealias FXMinY               = CGRectGetMinY

#else

  public typealias FXSize               = NSSize
  public let       FXSizeZero           = NSZeroSize
  public typealias FXPoint              = NSPoint
  public let       FXPointZero          = NSZeroPoint
  public typealias FXRect               = NSRect
  public let       FXRectZero           = NSZeroRect

  public func FXSizeMake(width:CGFloat, _ height:CGFloat) -> NSSize { return NSSizeMake(width, height) }
  public func FXRectMake(x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> NSRect { return NSRectMake(x, y, width, height) }
//  public func FXSizeMake           = FXSizeMake
  public func FXPointMake(x: CGFloat, _ y: CGFLoat) -> NSPoint { return NSMakePoint(x, y) }
//  public func FXRectMake           = NSMakeRect
//  public func FXEqualRects         = NSEqualRects
//  public func FXInsetRect          = NSInsetRect
//  public func FXUnionRect          = NSUnionRect
//  public func FXMaxX               = NSMaxX
//  public func FXMaxY               = NSMaxY
//  public func FXMinX               = NSMinX
//  public func FXMinY               = NSMinY

#endif

#if DEBUG
public func DebugLog(_ s:String) { NSLog(s) }
#else
public func DebugLog(_ s:String) {}
#endif
