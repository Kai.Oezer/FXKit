//
//  OverlayView.swift
//  FXKit
//
//  Created by Kai Özer on 8/7/14.
//  Copyright (c) 2014, 2017 Kai Özer. All rights reserved.
//

import UIKit

public class OverlayView : UIView
{
  private var label : UILabel

  public var message : String?
  {
    get { return label.text }
    set { label.text = newValue }
  }

  public override init(frame:CGRect)
  {
    label = UILabel(frame:frame)
    super.init(frame:frame)
    _configure()
  }

  public required init?(coder:NSCoder)
  {
    label = UILabel()
    super.init(coder:coder)
    _configure()
  }

  public convenience init(message:String)
  {
    self.init(frame:CGRect(x: 0,y: 0,width: 100,height: 20))
    label.text = message
  }

  private func _configure()
  {
    label.textAlignment = .center
    label.font = UIFont.preferredFont(forTextStyle: .headline)
    backgroundColor = UIColor(white:0.3, alpha:0.75)
    label.textColor = UIColor.white
    label.numberOfLines = 0 // makes it multi-line
    addSubview(label)
    addConstraint(NSLayoutConstraint(item:label, attribute:.centerX, relatedBy:.equal, toItem:self, attribute:.centerX, multiplier:1.0, constant:0.0))
    addConstraint(NSLayoutConstraint(item:label, attribute:.centerY, relatedBy:.equal, toItem:self, attribute:.centerY, multiplier:1.0, constant:0.0))
    self.layoutWithVisualFormats(["H:[label]", "V:[label]"], metricsInfo:nil, viewsInfo:["label":label])
  }
}
