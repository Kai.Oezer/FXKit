//
//  DraggingHandler.swift
//  FXKit
//
//  Created by Kai Özer on 8/10/14.
//  Copyright (c) 2014, 2017 Kai Özer. All rights reserved.
//

import UIKit

public protocol DraggingSource : AnyObject
{
  /// objects that adopt the DraggingItem protocol
  var draggedItems : [DraggingItem]? { get }

  /// the image used for dragging.
  var draggingImage : UIImage? { get }
}

public protocol DraggingDestination : AnyObject
{
  /// - returns: whether the given DraggingItem objects can be dropped
  func draggingDestinationAcceptsDraggedItems(_ items:[DraggingItem]) -> Bool

  /// perform a drop
  /// - parameter location: the location in target view coordinates
  func draggingDestinationHandleDroppedItems(_ items:[DraggingItem], forLocation location:CGPoint)
}

public protocol DraggingItem : AnyObject
{
  var draggingItemType : String { get }
}


private let USE_LONG_PRESS_GESTURE_FOR_DRAGGING = false


public class DraggingHandler<T:UIView> : NSObject where T:DraggingSource
{
  private weak var _sourceView : T?
  private var _gestureRecognizer : UIGestureRecognizer!
  private var _draggedItems = [DraggingItem]()
  private var _draggingView : UIImageView!
  private var _visibilityOffset = CGPoint.zero // otherwise the dragging image will be under the finger
  private var _visibilityOffsetIsSet : Bool = false
  private var _draggingStartLocation = CGPoint.zero

  public var enabled = true

  public init(draggingSource sourceView:T)
  {
    _sourceView = sourceView
    super.init()
    if USE_LONG_PRESS_GESTURE_FOR_DRAGGING
    {
      let longPressRecognizer = UILongPressGestureRecognizer(target:self, action:#selector(DraggingHandler.drag(_:)))
      longPressRecognizer.allowableMovement = 10000
      longPressRecognizer.minimumPressDuration = 0.1
      _gestureRecognizer = longPressRecognizer;
    }
    else
    {
      _gestureRecognizer = UIPanGestureRecognizer(target:self, action:#selector(DraggingHandler.drag(_:)))
      _gestureRecognizer.cancelsTouchesInView = false
    }
    _sourceView?.addGestureRecognizer(_gestureRecognizer)
  }

  deinit
  {
    _sourceView?.removeGestureRecognizer(_gestureRecognizer)
  }

  @objc func drag(_ recognizer:UIGestureRecognizer)
  {
    if enabled
    {
      let windowLocation = recognizer.location(in: nil)
      switch (recognizer.state)
      {
        case .began:
          _draggingStartLocation = windowLocation
          _visibilityOffset = CGPoint.zero
          _visibilityOffsetIsSet = false
          if let draggedItems = _sourceView?.draggedItems
          {
            _draggedItems = draggedItems
            _createDraggingViewAtWindowLocation(windowLocation)
          }

        case .changed:
          if !_visibilityOffsetIsSet
          {
            _updateVisibilityOffsetForDraggingLocation(windowLocation)
          }
          _draggingView.center = CGPoint(x: windowLocation.x + _visibilityOffset.x, y: windowLocation.y + _visibilityOffset.y);

        case .ended:
          _dropDraggedItemsAtWindowLocation(windowLocation)
          _draggedItems.removeAll()
          _draggingView.removeFromSuperview()
          _draggingView = nil;

        case .failed:
          DebugLog("Dragging failed");

        default: break
      }
    }
  }

  //MARK: Private

  private func _createDraggingViewAtWindowLocation(_ location:CGPoint)
  {
    if let draggingImage = _sourceView?.draggingImage
    {
      _draggingView = UIImageView(image:draggingImage)
      _draggingView.backgroundColor = UIColor.clear
      _sourceView?.window!.addSubview(_draggingView)
      _draggingView.center = location
      _rotateDraggingViewAccordingToScreenOrientation()
    }
  }

  private func _dropDraggedItemsAtWindowLocation(_ location:CGPoint)
  {
    let draggingViewLocation = CGPoint(x: location.x + _visibilityOffset.x, y: location.y + _visibilityOffset.y)
    if
      let targetView = _sourceView?.window?.hitTest(draggingViewLocation, with:nil),
      let destination = targetView as? DraggingDestination
    {
      if destination.draggingDestinationAcceptsDraggedItems(_draggedItems)
      {
        let viewLocation = targetView.convert(draggingViewLocation, from:nil)
        destination.draggingDestinationHandleDroppedItems(_draggedItems, forLocation:viewLocation)
      }
    }
  }

  private func _updateVisibilityOffsetForDraggingLocation(_ location:CGPoint)
  {
    let kThreshold : CGFloat = 20;
    if ( (abs(location.x - _draggingStartLocation.x) < kThreshold)
      && (abs(location.y - _draggingStartLocation.y) < kThreshold) )
    {
      _visibilityOffset = CGPoint(x: _draggingStartLocation.x - location.x, y: _draggingStartLocation.y - location.y);
    }
    else
    {
      _visibilityOffsetIsSet = true
    }
  }

	/// TODO: Needs to be reengineered to respond to viewWillTransition(:CGSize,:UIViewControllerTransitionCoordinator)
  private func _rotateDraggingViewAccordingToScreenOrientation()
  {
/*
    let orientation = UIApplication.shared.statusBarOrientation
    if orientation != .portrait
    {
      var angle : Double = 0.0
      switch orientation
      {
      case .portraitUpsideDown : angle = .pi
      case .landscapeRight: angle = .pi/2
      case .landscapeLeft: angle = -.pi/2
      default: angle = 0;
      }
      _draggingView.transform = CGAffineTransform(rotationAngle: CGFloat(angle));
    }
*/
  }

  private func _cancelDragging()
  {
    _gestureRecognizer.isEnabled = false
    _gestureRecognizer.isEnabled = true
  }
    
}
