//
//  FXKit.h
//  FXKit-iOS
//
//  Created by Kai Özer on 8/2/14.
//  Copyright (c) 2014, 2017 Kai Özer. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT double FXKitVersionNumber;
FOUNDATION_EXPORT const unsigned char FXKitVersionString[];

#import <FXKit/FXKit-Common.h>

// headers of Objective-C classes for iOS only
